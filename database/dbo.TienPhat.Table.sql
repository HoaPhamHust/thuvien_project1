USE [LibTest2]
GO
/****** Object:  Table [dbo].[TienPhat]    Script Date: 11/20/2018 10:24:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TienPhat](
	[MaPhat] [nchar](10) NOT NULL,
	[MaDG] [varchar](11) NOT NULL,
	[TienPhat] [nchar](10) NOT NULL,
 CONSTRAINT [PK_TienPhat] PRIMARY KEY CLUSTERED 
(
	[MaPhat] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TienPhat]  WITH CHECK ADD  CONSTRAINT [FK_TienPhat_DocGia] FOREIGN KEY([MaDG])
REFERENCES [dbo].[DocGia] ([MaDG])
GO
ALTER TABLE [dbo].[TienPhat] CHECK CONSTRAINT [FK_TienPhat_DocGia]
GO
