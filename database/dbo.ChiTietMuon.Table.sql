USE [LibTest2]
GO
/****** Object:  Table [dbo].[ChiTietMuon]    Script Date: 11/20/2018 10:24:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChiTietMuon](
	[MaMuon] [varchar](11) NOT NULL,
	[MaSachMuon] [varchar](11) NOT NULL,
	[NgayTra] [date] NOT NULL,
	[TienPhat] [float] NOT NULL,
	[id] [int] NOT NULL,
 CONSTRAINT [PK_ChiTietMuon] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ChiTietMuon]  WITH CHECK ADD  CONSTRAINT [FK_ChiTietMuon_MuonTra] FOREIGN KEY([MaMuon])
REFERENCES [dbo].[MuonTra] ([MaMuon])
GO
ALTER TABLE [dbo].[ChiTietMuon] CHECK CONSTRAINT [FK_ChiTietMuon_MuonTra]
GO
ALTER TABLE [dbo].[ChiTietMuon]  WITH CHECK ADD  CONSTRAINT [FK_ChiTietMuon_Sach] FOREIGN KEY([MaSachMuon])
REFERENCES [dbo].[Sach] ([MaSach])
GO
ALTER TABLE [dbo].[ChiTietMuon] CHECK CONSTRAINT [FK_ChiTietMuon_Sach]
GO
