USE [LibTest2]
GO
/****** Object:  Table [dbo].[MuonTra]    Script Date: 11/20/2018 10:24:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ARITHABORT ON
GO
CREATE TABLE [dbo].[MuonTra](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[MaMuon]  AS (CONVERT([varchar](11),'MT_'+right('000000'+CONVERT([varchar](4),[id]),(4)))) PERSISTED,
	[MaDocGiaMuon] [varchar](11) NOT NULL,
	[TenDocGiaMuon] [nchar](50) NOT NULL,
	[MaSachMuon] [nchar](10) NOT NULL,
	[TenSachMuon] [nchar](50) NOT NULL,
	[NgayMuon] [date] NOT NULL,
	[HanTra] [date] NOT NULL,
	[TrangThai] [nchar](30) NOT NULL,
 CONSTRAINT [PK_MuonTra] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_MuonTra] UNIQUE NONCLUSTERED 
(
	[MaMuon] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MuonTra]  WITH CHECK ADD  CONSTRAINT [FK_MuonTra_DocGia] FOREIGN KEY([MaDocGiaMuon])
REFERENCES [dbo].[DocGia] ([MaDG])
GO
ALTER TABLE [dbo].[MuonTra] CHECK CONSTRAINT [FK_MuonTra_DocGia]
GO
