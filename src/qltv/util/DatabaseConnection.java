/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qltv.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import qltv.common.JdbcTemplate;

/**
 *
 * @author dell
 */
public class DatabaseConnection {

    private static DatabaseConnection instance = new DatabaseConnection();
    DefaultTableModel dm = new DefaultTableModel();

    private DatabaseConnection() {

    }

    public static DatabaseConnection getInstance() {
        if (instance == null) {
            instance = new DatabaseConnection();
        }
        return instance;
    }

    public void updateTable(JTable table, String sql) {
        try {
//            DatabaseConnection.getConnection();
            ResultSet rs = JdbcTemplate.query(sql);
            table.setModel(DbUtils.resultSetToTableModel(rs));
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void updateTable(JTable table, String sql, String[] headers) {
        try {
            ResultSet rs = JdbcTemplate.query(sql);
            table.setModel(DbUtils.resultSetToTableModel(rs, headers));
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    // retrieve data for each column in the table
    public static Vector<String> select(String table, String[] key) {
        ResultSet rs = null;
        Vector<String> vector = new Vector<String>(100);
        String sqlcommand = "select * " + " from " + table;
        PreparedStatement pst = null;
        if (key == null) {
            sqlcommand = "SELECT * FROM " + table;
        } else {
            sqlcommand = "SELECT * FROM " + table + " WHERE ";
            for (int i = 0; i < key.length; i++) {
                sqlcommand = sqlcommand + key[i];
                if (i < key.length - 1) {
                    sqlcommand += " AND ";
                }
            }
            sqlcommand = sqlcommand + " ;";
        }
        try {
            rs = JdbcTemplate.query(sqlcommand);

            switch (table) {
                case "DocGia":
                    while (rs.next()) {

                        vector.add(rs.getString(1) + "\t" + rs.getString(2) + "\t" + rs.getString(3) + "\t" + rs.getString(4)
                                + "\t" + rs.getString(5) + "\t" + rs.getString(6) + "\t" + rs.getString(7));
                    }
                case "Sach":
                    while (rs.next()) {

                        vector.add(rs.getString(1) + "\t" + rs.getString(2) + "\t" + rs.getString(3) + "\t" + rs.getString(4)
                                + "\t" + rs.getString(5) + "\t" + rs.getString(6) + "\t" + rs.getString(7));
                    }
                case "MuonTra":
                    while (rs.next()) {

                        vector.add(rs.getString(1) + "\t" + rs.getString(2) + "\t" + rs.getString(3) + "\t" + rs.getString(4) + "\t"
                                + rs.getString(5) + "\t" + rs.getString(6) + "\t" + rs.getString(7) + "\t" + rs.getString(8));
                    }
                case "ChiTietMuon":
                    while (rs.next()) {

                        vector.add(rs.getString(1) + "\t" + rs.getString(2));
                    }

            }
        } catch (SQLException ex) {
            System.out.println("Select error !" + ex.toString());
        }

        return vector;
    }

    // thuc hien truy van khi tim kiem
    public static Vector<String> selectLike(String table, String key, String textField) {
        ResultSet rs = null;
        Vector<String> vector = new Vector<String>(100);
        String sqlcommand = "select * " + " from " + table;
        PreparedStatement pst = null;
        if (key == null) {
            sqlcommand = "SELECT * FROM " + table;
        } else {
            sqlcommand = "SELECT * FROM " + table + " WHERE " + key + " LIKE N'%" + textField + "%' ;";
            System.out.println(sqlcommand);

        }
        try {
            rs = JdbcTemplate.query(sqlcommand);

            switch (table) {
                case "DocGia":
                    while (rs.next()) {

                        vector.add(rs.getString(1) + "\t" + rs.getString(2) + "\t" + rs.getString(3) + "\t" + rs.getString(4)
                                + "\t" + rs.getString(5) + "\t" + rs.getString(6) + "\t" + rs.getString(7));
                    }
                case "Sach":
                    while (rs.next()) {

                        vector.add(rs.getString(1) + "\t" + rs.getString(2) + "\t" + rs.getString(3) + "\t" + rs.getString(4)
                                + "\t" + rs.getString(5) + "\t" + rs.getString(6) + "\t" + rs.getString(7));
                    }
                case "MuonTra":
                    while (rs.next()) {

                        vector.add(rs.getString(1) + "\t" + rs.getString(2) + "\t" + rs.getString(3) + "\t" + rs.getString(4) + "\t"
                                + rs.getString(4) + "\t" + rs.getString(5) + "\t" + rs.getString(6) + "\t" + rs.getString(7) + "\t" + rs.getString(8));
                    }
                case "ChiTietMuonTra":
                    while (rs.next()) {

                        vector.add(rs.getString(1) + "\t" + rs.getString(2));
                    }
                case "NhanVien":
                    while (rs.next()) {

                        vector.add(rs.getString(1) + "\t" + rs.getString(2) + "\t" + rs.getString(3) + "\t" + rs.getString(4) + "\t"
                                + rs.getString(4) + "\t" + rs.getString(5));
                    }

            }
        } catch (SQLException ex) {
            System.out.println("Select error !" + ex.toString());
        }

        return vector;
    }

    //
    // insert data into table in SQL Server
    public int insert(String table, String[] values) {

        String sqlcommand = "INSERT INTO " + table;
        PreparedStatement pst = null;

        if (table.equals("DocGia")) {
            sqlcommand = sqlcommand + " VALUES(" + "'" + values[0] + "',N'" + values[1] + "','" + values[2] + "','" + values[3]
                    + "',N'" + values[4] + "','" + values[5] + "','" + values[6] + "');";
        } else if (table.equals("Sach")) {
            sqlcommand = sqlcommand + " VALUES(" + "'" + values[0] + "',N'" + values[1] + "',N'" + values[2] + "',N'" + values[3]
                    + "',N'" + values[4] + "','" + values[5] + "','" + values[6] + "');";
        } else if (table.equals("MuonTra")) {
            sqlcommand = sqlcommand + " values (" + "'" + values[0] + "', '" + values[1] + "', '" + values[2] + "', '" + values[3]
                    + "', N'" + values[4] + "');";
        } else if (table.equals("ChiTietMuonTra")) {
            sqlcommand = sqlcommand + table + " values (" + "'" + values[0] + "','" + values[1] + "','" + values[2] + "');";
        } else if (table.equals("NhanVien")) {
            sqlcommand = sqlcommand + " VALUES(" + "'" + values[0] + "',N'" + values[1] + "','" + values[2]
                    + "',N'" + values[3] + "','" + values[4] + "');";
        }
        System.out.println(sqlcommand);
        return JdbcTemplate.update(sqlcommand, null);
    }

    // delete column data into table in SQL Server
    public int delete(String table, String columnName, String key) {

        String sqlcommand = " delete from " + table + " where " + columnName + " ='" + key + "';";
        //System.out.println(sqlcommand);

        return JdbcTemplate.update(sqlcommand, null);
    }

    // update data in table with record have columnname = key
    public int update(String table, String[] values, String key) {

        String sqlcommand = " update " + table + " set ";
        PreparedStatement pst = null;

        for (int i = 0; i < values.length; i++) {
            sqlcommand += values[i];
            if (i < values.length - 1) {
                sqlcommand += ",";
            }
        }
        sqlcommand += " where " + key + ";";
        System.out.println(sqlcommand);

        return JdbcTemplate.update(sqlcommand, null);
    }

}
