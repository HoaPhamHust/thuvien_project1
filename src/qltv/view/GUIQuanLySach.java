package qltv.view;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import static javax.swing.JOptionPane.OK_CANCEL_OPTION;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import qltv.common.JdbcTemplate;
import qltv.controler.CSach;

import qltv.model.dto.Sach;
import qltv.util.DatabaseConnection;
import qltv.util.PrintPDF;

/**
 *
 * @author dell
 */
public class GUIQuanLySach extends javax.swing.JPanel {

    DefaultTableModel dm;
    CSach cSach = new CSach();
    ResultSet rs = null;
    SimpleDateFormat sdf = new SimpleDateFormat();
    List<Sach> sachList = null;

    public GUIQuanLySach() {
        initComponents();
        loadDataInTable();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel5 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        tfMaSach = new javax.swing.JTextField();
        tfTenSach = new javax.swing.JTextField();
        tfNhaXuatBan = new javax.swing.JTextField();
        tfSoLuong = new javax.swing.JTextField();
        tfDonGia = new javax.swing.JTextField();
        tfTacGia = new javax.swing.JTextField();
        tfTheLoai = new javax.swing.JTextField();
        jPanel7 = new javax.swing.JPanel();
        btnThem = new javax.swing.JButton();
        btnXoa = new javax.swing.JButton();
        btnSua = new javax.swing.JButton();
        btnRefresh = new javax.swing.JButton();
        btnThemTuFile = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        tfTimKiem = new javax.swing.JTextField();
        cbbTimKiem = new javax.swing.JComboBox<>();
        btnTimKiem = new javax.swing.JButton();
        btnXuatBieuMau = new javax.swing.JButton();
        XuatPDF = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbSach = new javax.swing.JTable();

        setMaximumSize(new java.awt.Dimension(1020, 490));
        setMinimumSize(new java.awt.Dimension(1020, 490));
        setName(""); // NOI18N
        setPreferredSize(new java.awt.Dimension(1020, 490));
        setLayout(new javax.swing.BoxLayout(this, javax.swing.BoxLayout.PAGE_AXIS));

        jPanel5.setMaximumSize(new java.awt.Dimension(32767, 74));
        jPanel5.setMinimumSize(new java.awt.Dimension(224, 74));
        jPanel5.setPreferredSize(new java.awt.Dimension(1020, 74));

        jLabel1.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/qltv/image/Abooks.png"))); // NOI18N
        jLabel1.setText("QUẢN LÝ SÁCH");
        jPanel5.add(jLabel1);

        add(jPanel5);

        jPanel4.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 15, 1, 15));
        jPanel4.setMaximumSize(new java.awt.Dimension(33537, 333));
        jPanel4.setMinimumSize(new java.awt.Dimension(770, 333));
        jPanel4.setPreferredSize(new java.awt.Dimension(1020, 333));
        jPanel4.setLayout(new javax.swing.BoxLayout(jPanel4, javax.swing.BoxLayout.LINE_AXIS));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 2), "Thông tin sách"));
        jPanel1.setMaximumSize(new java.awt.Dimension(740, 333));
        jPanel1.setMinimumSize(new java.awt.Dimension(740, 333));
        jPanel1.setPreferredSize(new java.awt.Dimension(740, 333));
        jPanel1.setLayout(new java.awt.BorderLayout());

        jLabel2.setText("Mã sách : ");

        jLabel3.setText("Số lượng :");

        jLabel4.setText("Tên sách : ");

        jLabel5.setText("Nhà xuất bản : ");

        jLabel6.setText("Tác giả : ");

        jLabel7.setText("Đơn giá : ");

        jLabel8.setText("Thể loại : ");

        tfMaSach.setEditable(false);

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 728, Short.MAX_VALUE)
            .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel6Layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel4)
                        .addComponent(jLabel2)
                        .addComponent(jLabel5)
                        .addComponent(jLabel3))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(jPanel6Layout.createSequentialGroup()
                            .addComponent(tfSoLuong, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(281, 281, 281))
                        .addGroup(jPanel6Layout.createSequentialGroup()
                            .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(tfNhaXuatBan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(tfTenSach, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(tfMaSach, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 115, Short.MAX_VALUE)
                            .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(jPanel6Layout.createSequentialGroup()
                                    .addComponent(jLabel7)
                                    .addGap(18, 18, 18)
                                    .addComponent(tfDonGia, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel6Layout.createSequentialGroup()
                                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel8)
                                        .addComponent(jLabel6))
                                    .addGap(18, 18, 18)
                                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(tfTacGia)
                                        .addComponent(tfTheLoai, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                    .addContainerGap()))
        );

        jPanel6Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {tfMaSach, tfNhaXuatBan, tfSoLuong, tfTenSach});

        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 230, Short.MAX_VALUE)
            .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel6Layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel6Layout.createSequentialGroup()
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(tfDonGia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel7))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(tfTacGia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel6))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(tfTheLoai, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel8)))
                        .addGroup(jPanel6Layout.createSequentialGroup()
                            .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(tfMaSach, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel2))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(tfTenSach, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel4))
                            .addGap(10, 10, 10)
                            .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(tfNhaXuatBan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel5))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(tfSoLuong, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel3))))
                    .addContainerGap(68, Short.MAX_VALUE)))
        );

        jPanel6Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {tfDonGia, tfMaSach, tfNhaXuatBan, tfSoLuong, tfTacGia, tfTenSach, tfTheLoai});

        jPanel1.add(jPanel6, java.awt.BorderLayout.CENTER);

        jPanel7.setBorder(javax.swing.BorderFactory.createEmptyBorder(20, 20, 20, 20));
        jPanel7.setMaximumSize(new java.awt.Dimension(32767, 100));
        jPanel7.setMinimumSize(new java.awt.Dimension(561, 100));
        jPanel7.setLayout(new java.awt.GridLayout(1, 0, 10, 0));

        btnThem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/qltv/image/Add-icon.png"))); // NOI18N
        btnThem.setText("Thêm ");
        btnThem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnThemActionPerformed(evt);
            }
        });
        jPanel7.add(btnThem);

        btnXoa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/qltv/image/Atru.png"))); // NOI18N
        btnXoa.setText("Xóa");
        btnXoa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnXoaActionPerformed(evt);
            }
        });
        jPanel7.add(btnXoa);

        btnSua.setIcon(new javax.swing.ImageIcon(getClass().getResource("/qltv/image/Asua.png"))); // NOI18N
        btnSua.setText("Sửa");
        btnSua.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSuaActionPerformed(evt);
            }
        });
        jPanel7.add(btnSua);

        btnRefresh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/qltv/image/Alammoi.png"))); // NOI18N
        btnRefresh.setText("Làm mới");
        btnRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshActionPerformed(evt);
            }
        });
        jPanel7.add(btnRefresh);

        btnThemTuFile.setIcon(new javax.swing.ImageIcon(getClass().getResource("/qltv/image/Files-icon.png"))); // NOI18N
        btnThemTuFile.setText("Thêm file");
        btnThemTuFile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnThemTuFileActionPerformed(evt);
            }
        });
        jPanel7.add(btnThemTuFile);

        jPanel1.add(jPanel7, java.awt.BorderLayout.SOUTH);

        jPanel4.add(jPanel1);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 2), "Tìm kiếm sách"));
        jPanel2.setMaximumSize(new java.awt.Dimension(32767, 333));
        jPanel2.setPreferredSize(new java.awt.Dimension(325, 158));

        jLabel9.setText("Nhập từ khóa tìm kiếm : ");

        cbbTimKiem.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Mã sách", "Tên sách", "Tác giả", "Nhà xuất bản", "Thể loại", "None", " ", " " }));

        btnTimKiem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/qltv/image/Atim.png"))); // NOI18N
        btnTimKiem.setText("Tìm kiếm");
        btnTimKiem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTimKiemActionPerformed(evt);
            }
        });

        btnXuatBieuMau.setIcon(new javax.swing.ImageIcon(getClass().getResource("/qltv/image/Athongke.png"))); // NOI18N
        btnXuatBieuMau.setText("Xuất biểu mẫu");
        btnXuatBieuMau.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnXuatBieuMauActionPerformed(evt);
            }
        });

        XuatPDF.setIcon(new javax.swing.ImageIcon(getClass().getResource("/qltv/image/Apdf.png"))); // NOI18N
        XuatPDF.setText("Xuất pdf");
        XuatPDF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                XuatPDFActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel9)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(btnTimKiem, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(tfTimKiem, javax.swing.GroupLayout.DEFAULT_SIZE, 144, Short.MAX_VALUE))
                        .addGap(28, 28, 28)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cbbTimKiem, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnXuatBieuMau, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(0, 35, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(XuatPDF)
                .addGap(111, 111, 111))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(jLabel9)
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfTimKiem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbbTimKiem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnTimKiem)
                    .addComponent(btnXuatBieuMau))
                .addGap(18, 18, 18)
                .addComponent(XuatPDF)
                .addContainerGap(103, Short.MAX_VALUE))
        );

        jPanel4.add(jPanel2);

        add(jPanel4);

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Danh sách sách"));

        jScrollPane1.setBackground(new java.awt.Color(255, 255, 255));

        tbSach.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tbSach.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbSachMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tbSach);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1093, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 180, Short.MAX_VALUE)
                .addContainerGap())
        );

        add(jPanel3);
    }// </editor-fold>//GEN-END:initComponents

    private void loadDataInTable() {

        dm = new DefaultTableModel();
        sachList = new ArrayList<Sach>();
        Vector<String> colName = new Vector<>();
        dm.addColumn("Mã sách");
        dm.addColumn("Tên sách");
        dm.addColumn("Nhà xuất bản");
        dm.addColumn("Tác giả");
        dm.addColumn("Thể loại");
        dm.addColumn("Đơn giá");
        dm.addColumn("Số lượng");

        cSach = new CSach();
        sachList = cSach.selectAll();

        for (Sach s : sachList) {
            Vector vector = new Vector();

            vector.add(s.getMaSach());
            vector.add(s.getTenSach());
            vector.add(s.getNxb());
            vector.add(s.getTacGia());
            vector.add(s.getTheLoai());
            vector.add(s.getDonGia());
            vector.add(s.getSoLuong());

            // tao hang
            dm.addRow(vector);
        }
        // Dua DL tu dm vao table
        tbSach.setModel(dm);

    }

    private void loadDateSearch() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        //khởi tạo lại model, xóa hết cột cũ dòng cũ đi
        dm = new DefaultTableModel();
        Vector<String> colName = new Vector<>();
        //tạo cột
        dm.addColumn("Mã sách");
        dm.addColumn("Tên sách");
        dm.addColumn("Nhà xuất bản");
        dm.addColumn("Tác giả");
        dm.addColumn("Thể loại");
        dm.addColumn("Đơn giá");
        dm.addColumn("Số lượng");

        // lay ra toan bo danh sach doc gia
        cSach = new CSach();

        Sach sach = new Sach();
        sach.setMaSach(tfTimKiem.getText());
        sach.setTenSach(tfTimKiem.getText());
        sach.setNxb(tfTimKiem.getText());
        sach.setTacGia(tfTimKiem.getText());
        sach.setTheLoai(tfTimKiem.getText());

        String result = (String) cbbTimKiem.getSelectedItem();
        System.out.println("result search = " + result);

        if (result.equals("Mã sách")) {
            try {
                sachList = cSach.search(sach, result);
            } catch (SQLException ex) {
                Logger.getLogger(GUIQuanLySach.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (result.equals("Tên sách")) {
            try {
                sachList = cSach.search(sach, result);
            } catch (SQLException ex) {
                Logger.getLogger(GUIQuanLySach.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (result.equals("Nhà xuất bản")) {
            try {
                sachList = cSach.search(sach, result);
            } catch (SQLException ex) {
                Logger.getLogger(GUIQuanLySach.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (result.equals("Tác giả")) {
            try {
                sachList = cSach.search(sach, result);
            } catch (SQLException ex) {
                Logger.getLogger(GUIQuanLySach.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (result.equals("Thể loại")) {
            try {
                sachList = cSach.search(sach, result);
            } catch (SQLException ex) {
                Logger.getLogger(GUIQuanLySach.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (result.equals("None")) {
            try {
                sachList = cSach.search(sach, result);
            } catch (SQLException ex) {
                Logger.getLogger(GUIQuanLySach.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        // add toan bo DL tu CSDL vao table
        for (Sach s : sachList) {
            Vector vector = new Vector();
            vector.add(s.getMaSach());
            vector.add(s.getTenSach());
            vector.add(s.getNxb());
            vector.add(s.getTacGia());
            vector.add(s.getTheLoai());
            vector.add(s.getDonGia());
            vector.add(s.getSoLuong());

            // tao hang
            dm.addRow(vector);
        }
        // Dua DL tu dm vao table
        tbSach.setModel(dm);

    }

    private void clear() {
        tfMaSach.setText("");
        tfTenSach.setText("");
        tfNhaXuatBan.setText("");
        tfTacGia.setText("");
        tfTheLoai.setText("");
        tfDonGia.setText("");
        tfSoLuong.setText("");
    }
// sai

    private void themFile() {
        JFileChooser chooser = new JFileChooser();
        chooser.setCurrentDirectory(new File("D:\\"));
        int check = chooser.showOpenDialog(this);
        if (check == chooser.APPROVE_OPTION) {

            try {
                String[] titile = new String[]{"Mã sách", "Tên sách", "Nhà xuất bản", "Tác giả", "Thể loại", "Đơn giá", "Số lượng"};

                dm = new DefaultTableModel(titile, 0);
                File file = chooser.getSelectedFile();

                Workbook wb = Workbook.getWorkbook(file);
                Sheet sheet = wb.getSheet(0);

                Cell cell;

                //List<Sach> sachList = null;
                DatabaseConnection conn = DatabaseConnection.getInstance();
                String[] values = new String[7];
                for (int i = 1; i < sheet.getRows(); i++) {
                    Sach s = new Sach();

                    cell = sheet.getCell(0, i);
                    values[0] = cell.getContents();
                    //s.setMaSach(cell.getContents());
                    cell = sheet.getCell(1, i);
                    values[1] = cell.getContents();
                    //s.setTenSach(cell.getContents());
                    cell = sheet.getCell(2, i);
                    values[2] = cell.getContents();
                    //s.setHanSuDung(sdf.parse(cell.getContents()));
                    cell = sheet.getCell(3, i);
                    values[3] = cell.getContents();
                    //s.setNgaySinh(sdf.parse(cell.getContents())); // convert String -> Date in java
                    cell = sheet.getCell(4, i);
                    values[4] = cell.getContents();
                    // s.setDiaChi(cell.getContents());
                    cell = sheet.getCell(5, i);
                    values[5] = cell.getContents();
                    //s.setSoDienThoai(cell.getContents());
                    cell = sheet.getCell(6, i);
                    values[6] = cell.getContents();
                    //s.setGioiTinh(cell.getContents());

//                    sachList.add(s);
//                    csach.insert(s); // insert DL vao trong CSDL
                    conn.insert("Sach", values); // insert DL vao trong CSDL
                    dm.addRow(values);
                }
//                Vector v = new Vector(sachList);
//                dm.addRow(v);

                tbSach.setModel(dm);
            } catch (IOException ex) {
                Logger.getLogger(GUIQuanLySach.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(null, "Lỗi IOException");
            } catch (BiffException ex) {
                Logger.getLogger(GUIQuanLySach.class.getName()).log(Level.SEVERE, null, ex);

            }
        }
    }

    private void writeFileExcel() {

        WritableWorkbook wb;

        try {
            wb = Workbook.createWorkbook(new File("D:\\ouputSach.xls"));
            WritableSheet sheet = wb.createSheet("Sach", 0);
            try {
                sheet.addCell(new jxl.write.Label(0, 0, "DANH SACH SACH"));
                sheet.addCell(new jxl.write.Label(0, 1, "MaSach")); // cột 0 d
                sheet.addCell(new jxl.write.Label(1, 1, "TenSach"));
                sheet.addCell(new jxl.write.Label(2, 1, "NXB"));
                sheet.addCell(new jxl.write.Label(3, 1, "TacGia"));
                sheet.addCell(new jxl.write.Label(4, 1, "TheLoai"));
                sheet.addCell(new jxl.write.Label(5, 1, "DonGia"));
                sheet.addCell(new jxl.write.Label(6, 1, "SoLuong"));
                int rowBegin = 2;
                TableModel tableModel = tbSach.getModel();
                for (int row = rowBegin, i = 0; row < rowBegin + tbSach.getRowCount(); row++, i++) {
                    sheet.addCell(new jxl.write.Label(0, row, (String) tableModel.getValueAt(i, 0)));
                    sheet.addCell(new jxl.write.Label(1, row, (String) tableModel.getValueAt(i, 1)));
                    sheet.addCell(new jxl.write.Label(2, row, (String) tableModel.getValueAt(i, 2)));
                    sheet.addCell(new jxl.write.Label(3, row, (String) tableModel.getValueAt(i, 3)));
                    sheet.addCell(new jxl.write.Label(4, row, (String) tableModel.getValueAt(i, 4)));
                    sheet.addCell(new jxl.write.Label(5, row, (String) tableModel.getValueAt(i, 5)));
                    sheet.addCell(new jxl.write.Label(6, row, (String) tableModel.getValueAt(i, 6)));

                }
                wb.write();
                wb.close();
                JOptionPane.showMessageDialog(null, "Write to excel success!");
            } catch (WriteException ex) {
                Logger.getLogger(GUIQuanLySach.class.getName()).log(Level.SEVERE, null, ex);
            }

        } catch (IOException ex) {
            Logger.getLogger(GUIQuanLySach.class.getName()).log(Level.SEVERE, null, ex);
        }

    }


    private void btnXoaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnXoaActionPerformed
        //Hiển thị confirm
        int confirm = JOptionPane.showConfirmDialog(this, "Bạn có muốn xóa không?", "Thông báo", OK_CANCEL_OPTION);
        String sql = "SELECT DISTINCT ctm.MaSachMuon FROM ChiTietMuon ctm WHERE ctm.MaSachMuon = '" + tfMaSach.getText().trim() + "'";
//Nếu chọn Yes
        if (confirm == JOptionPane.YES_OPTION) {

            try {
                rs = JdbcTemplate.query(sql);
                if (rs.next()) {
                    JOptionPane.showMessageDialog(null, "Sách đang được mượn");
                } else {
                    Sach sach = new Sach(tfMaSach.getText());
                    boolean result = cSach.delete(sach);
                    if (result == true) {
                        JOptionPane.showMessageDialog(this, "Xóa sách thành công!");
                    }
                }

                clear();
                loadDataInTable();
            } catch (SQLException ex) {
                Logger.getLogger(GUIQuanLySach.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_btnXoaActionPerformed

    private void btnThemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnThemActionPerformed
        int result = JOptionPane.showConfirmDialog(this, "Bạn muốn thêm dữ liệu ?", "Thêm sách", JOptionPane.YES_NO_OPTION);
        if (result == JOptionPane.YES_OPTION) {

            if (tfMaSach.getText().isEmpty()) {
                JOptionPane.showMessageDialog(this, "Chưa nhập mã sách");
                return;
            }
            if (tfTenSach.getText().isEmpty()) {
                JOptionPane.showMessageDialog(this, "Chưa nhập tên sách");
                return;
            }
            if (tfNhaXuatBan.getText().isEmpty()) {
                JOptionPane.showMessageDialog(this, "Chưa nhập nhà xuất bản");
                return;
            }
            if (tfDonGia.getText().isEmpty()) {
                JOptionPane.showMessageDialog(this, "Chưa nhập đơn giá");
                return;
            }
            if (tfSoLuong.getText().isEmpty()) {
                JOptionPane.showMessageDialog(this, "Chưa nhập số lượng");
                return;
            }

            String sql = "SELECT MaSach FROM Sach WHERE MaSach = '" + tfMaSach.getText() + "'";
            try {
                rs = JdbcTemplate.query(sql);
            } catch (SQLException ex) {
                Logger.getLogger(GUIQuanLySach.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                if (!rs.next()) {
                    Sach s = new Sach();
                    s.setMaSach(tfMaSach.getText());
                    s.setTenSach(tfTenSach.getText());
                    s.setNxb(tfNhaXuatBan.getText());
                    s.setTacGia(tfTacGia.getText());
                    s.setTheLoai(tfTheLoai.getText());
                    s.setDonGia(Float.parseFloat(tfDonGia.getText()));
                    System.out.println(Float.parseFloat(tfDonGia.getText()));
                    s.setSoLuong(Integer.parseInt(tfSoLuong.getText()));
                    

                    boolean inS = cSach.insert(s);
                    if (inS) {
                        JOptionPane.showMessageDialog(this, "Thêm sách thành công!");
                    }
                    loadDataInTable();
                    
                }
                else{
                JOptionPane.showMessageDialog(null, "Mã sách đã tồn tại");
                }
                } catch (SQLException ex) {
                Logger.getLogger(GUIQuanLySach.class.getName()).log(Level.SEVERE, null, ex);
                
            }
        }
    }//GEN-LAST:event_btnThemActionPerformed

    private void btnSuaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSuaActionPerformed
        int result = JOptionPane.showConfirmDialog(this, "Bạn muốn thêm dữ liệu ?", "Thêm sách", JOptionPane.YES_NO_OPTION);
        if (result == JOptionPane.YES_OPTION) {
            if (tfMaSach.getText().isEmpty()) {
                JOptionPane.showMessageDialog(this, "Chưa nhập mã sách");
                return;
            }

            Sach s = new Sach();
            s.setMaSach(tfMaSach.getText() != null ? tfMaSach.getText() : "");
            s.setTenSach(tfTenSach.getText() != null ? tfTenSach.getText() : "");
            s.setNxb(tfNhaXuatBan.getText() != null ? tfNhaXuatBan.getText() : "");
            s.setTacGia(tfTacGia.getText() != null ? tfTacGia.getText() : "");
            s.setTheLoai(tfTheLoai.getText() != null ? tfTheLoai.getText() : "");
            s.setDonGia(Float.parseFloat(tfDonGia.getText()));
            s.setSoLuong(Integer.parseInt(tfSoLuong.getText()));

            boolean inS = cSach.update(s);
            if (inS) {
                JOptionPane.showMessageDialog(this, "Sửa sách thành công!");
            }
            loadDataInTable();
        }

    }//GEN-LAST:event_btnSuaActionPerformed

    private void btnRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshActionPerformed
        loadDataInTable();
    }//GEN-LAST:event_btnRefreshActionPerformed

    private void btnThemTuFileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnThemTuFileActionPerformed
        themFile();
    }//GEN-LAST:event_btnThemTuFileActionPerformed

    private void btnXuatBieuMauActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnXuatBieuMauActionPerformed
        writeFileExcel();
    }//GEN-LAST:event_btnXuatBieuMauActionPerformed

    private void tbSachMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbSachMouseClicked
        int i = tbSach.getSelectedRow();
        TableModel model = tbSach.getModel();
        tfMaSach.setText((String) model.getValueAt(i, 0));
        tfTenSach.setText((String) model.getValueAt(i, 1));
        tfNhaXuatBan.setText((String) model.getValueAt(i, 2));
        tfTacGia.setText((String) model.getValueAt(i, 3));
        tfTheLoai.setText((String) model.getValueAt(i, 4));
        tfDonGia.setText(((Float) model.getValueAt(i, 5)).toString());
        tfSoLuong.setText(((Integer) model.getValueAt(i, 6)).toString());
    }//GEN-LAST:event_tbSachMouseClicked

    private void btnTimKiemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTimKiemActionPerformed
        loadDateSearch();
    }//GEN-LAST:event_btnTimKiemActionPerformed

    private void XuatPDFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_XuatPDFActionPerformed
        String[] header = {"Mã sách", "Tên sách", "Nhà xuất bản", "Tác giả", "Thể loại", "Đơn giá", "Số lượng"};

        loadDateSearch();
        String[][] data = new String[sachList.size()][];
        String str = null;

        for (int i = 0; i < sachList.size(); i++) {
            //str = sachList.get(i).getMaSach() + "\t" + sachList.get(i).getTenSach() + "\t" + v sachList.get(i).getNxb()+ "\t" + sachList.get(i).getTacGia() + "\t" + sachList.get(i).getTheLoai() + "\t" + sachList.get(i).getDonGia() + "\t" + sachList.get(i).getSoLuong();
            str = sachList.get(i).getMaSach() + "\t" + sachList.get(i).getTenSach() + "\t" + sachList.get(i).getNxb() + "\t"
                    + sachList.get(i).getTacGia() + "\t" + sachList.get(i).getTheLoai() + "\t" + sachList.get(i).getDonGia() + "\t" + sachList.get(i).getSoLuong();
            data[i] = str.split("\t");
        }

        PrintPDF print = new PrintPDF();
        JFileChooser fc = new JFileChooser();
        int retirnval = fc.showSaveDialog(this);
        if (retirnval == JFileChooser.APPROVE_OPTION) {
            try {
                File file = fc.getSelectedFile();
                print.printSearchReport(file.getPath(), "Sach", header, data, (String) cbbTimKiem.getSelectedItem(), tfTimKiem.getText(), "sách");

            } catch (IOException ex) {
                Logger.getLogger(GUIQuanLySach.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_XuatPDFActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton XuatPDF;
    private javax.swing.JButton btnRefresh;
    private javax.swing.JButton btnSua;
    private javax.swing.JButton btnThem;
    private javax.swing.JButton btnThemTuFile;
    private javax.swing.JButton btnTimKiem;
    private javax.swing.JButton btnXoa;
    private javax.swing.JButton btnXuatBieuMau;
    private javax.swing.JComboBox<String> cbbTimKiem;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tbSach;
    private javax.swing.JTextField tfDonGia;
    private javax.swing.JTextField tfMaSach;
    private javax.swing.JTextField tfNhaXuatBan;
    private javax.swing.JTextField tfSoLuong;
    private javax.swing.JTextField tfTacGia;
    private javax.swing.JTextField tfTenSach;
    private javax.swing.JTextField tfTheLoai;
    private javax.swing.JTextField tfTimKiem;
    // End of variables declaration//GEN-END:variables
}
