/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qltv.view;

import java.awt.BorderLayout;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import qltv.common.JdbcTemplate;
import qltv.util.DatabaseConnection;
import qltv.util.PrintPDF;

/**
 *
 * @author dell
 */
public class GUIThongKeBaoCao extends javax.swing.JPanel {

    ResultSet rs = null;
    Vector<String> vec1 = new Vector<String>();
    Vector<String> vec2 = new Vector<String>();
    Vector<String> vec3 = new Vector<String>();

    PanelDocGia pDocGia;
    PanelSach pSach;
    PanelMuonTra pMuonTra;

    public GUIThongKeBaoCao() {
        initComponents();
        pDocGia = new PanelDocGia();
        pSach = new PanelSach();
        pMuonTra = new PanelMuonTra();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        btnThongKe = new javax.swing.JButton();
        jcbThongKe = new javax.swing.JComboBox<>();
        tfTuNgay = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        tfDenNgay = new javax.swing.JTextField();
        btnXuatBaoCao = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        pThongKe = new javax.swing.JPanel();

        setBorder(javax.swing.BorderFactory.createEmptyBorder(15, 15, 15, 15));
        setMaximumSize(new java.awt.Dimension(1020, 490));
        setMinimumSize(new java.awt.Dimension(1020, 490));
        setLayout(new java.awt.BorderLayout());

        btnThongKe.setText("Thống kê");
        btnThongKe.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnThongKeActionPerformed(evt);
            }
        });

        jcbThongKe.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Độc giả", "Sách", "Mượn trả" }));

        tfTuNgay.setEnabled(false);

        jLabel4.setText("Đến ngày :");

        jLabel3.setText("Từ ngày :");

        jLabel2.setText("Thống kê theo : ");

        tfDenNgay.setEnabled(false);

        btnXuatBaoCao.setText("Xuất báo cáo");
        btnXuatBaoCao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnXuatBaoCaoActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        jLabel1.setText("THỐNG KÊ - BÁO CÁO");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(122, 122, 122)
                .addComponent(jLabel1)
                .addContainerGap(133, Short.MAX_VALUE))
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                            .addGap(66, 66, 66)
                            .addComponent(jLabel2)
                            .addGap(40, 40, 40)
                            .addComponent(jcbThongKe, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel2Layout.createSequentialGroup()
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel2Layout.createSequentialGroup()
                                    .addComponent(jLabel3)
                                    .addGap(18, 18, 18)
                                    .addComponent(tfTuNgay, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                    .addComponent(btnThongKe)
                                    .addGap(11, 11, 11)))
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel2Layout.createSequentialGroup()
                                    .addGap(30, 30, 30)
                                    .addComponent(jLabel4)
                                    .addGap(18, 18, 18)
                                    .addComponent(tfDenNgay, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel2Layout.createSequentialGroup()
                                    .addGap(18, 18, 18)
                                    .addComponent(btnXuatBaoCao)))))
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addContainerGap(110, Short.MAX_VALUE))
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addGap(41, 41, 41)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jcbThongKe, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel2))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel3)
                        .addComponent(tfTuNgay, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel4)
                        .addComponent(tfDenNgay, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(19, 19, 19)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnXuatBaoCao)
                        .addComponent(btnThongKe))
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        jPanel1.add(jPanel2);

        add(jPanel1, java.awt.BorderLayout.NORTH);

        pThongKe.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Thông tin thống kê"));
        pThongKe.setLayout(new java.awt.BorderLayout());
        add(pThongKe, java.awt.BorderLayout.CENTER);

        getAccessibleContext().setAccessibleName("");
    }// </editor-fold>//GEN-END:initComponents

    //////////// taGia,  theLoai, NXB
    public Vector<String> NameAndSoLuong(String sql) {
        Vector<String> vector = new Vector<String>(100);
        try {
            rs = JdbcTemplate.query(sql);
            while (rs.next()) {
                vector.add(rs.getString(1) + "\t" + rs.getInt(2));
            }
        } catch (SQLException ex) {
            Logger.getLogger(GUIThongKeBaoCao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return vector;
    }
//////////////

    public Vector<String> dsDGMuonSach() {
        Vector<String> vector = new Vector<String>(100);
        String sql = "";
        try {

            sql = "select mt.MaMuon, MaDocGiaMuon, TenDocGiaMuon, MaSachMuon, TenSach   from MuonTra mt, ChiTietMuon ctm, Sach s "
                    + " where mt.MaMuon = ctm.MaMuon AND ctm.MaSachMuon = s.MaSach AND TrangThai = N'Đang mượn'";
            rs = JdbcTemplate.query(sql);
            while (rs.next()) {
                vector.add(rs.getInt(1) + "\t" + rs.getString(2) + "\t" + rs.getString(3) + "\t" + rs.getString(4) + "\t" + rs.getString(5));
            }

        } catch (SQLException ex) {
            Logger.getLogger(GUIThongKeBaoCao.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Loi loi loiiiii" + ex.toString());
        }
        return vector;
    }
///////

    public Vector<String> baTenSachMuonnhieuNhat() {
        Vector<String> vector = new Vector<String>(100);
        String sql = "";
        try {

            sql = "select top 3 MaSachMuon, s.TenSach, count(MaSachMuon) as SoLuong from ChiTietMuon ctm, Sach s "
                    + " where ctm.MaSachMuon = s.MaSach group by MaSachMuon, TenSach order by SoLuong DESC;";
            rs = JdbcTemplate.query(sql);
            while (rs.next()) {
                vector.add(rs.getString(1) + "\t" + rs.getString(2) + "\t" + rs.getInt(3));
            }

        } catch (SQLException ex) {
            Logger.getLogger(GUIThongKeBaoCao.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Loi loi loiiiii" + ex.toString());
        }
        return vector;
    }

/////
    private void btnXuatBaoCaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnXuatBaoCaoActionPerformed
        String result = (String) jcbThongKe.getSelectedItem();
        if (result.equals("Độc giả")) {
            int total = 0, sumDocGia = 0, i = 0;
            try {
                rs = JdbcTemplate.query("SELECT COUNT(MaDG) FROM DocGia");
                if (rs.next()) {
                    total = rs.getInt(1); // lay tong so ma sach
                    System.out.println(total);
                }

                String[] gioiTinh = new String[2];
                rs = JdbcTemplate.query("SELECT COUNT(GioiTinh) FROM DocGia "
                        + " GROUP BY GioiTinh;");

                while (rs.next()) {
                    gioiTinh[i] = rs.getString(1);
                    i++;
                }

                // theo dia chi
                String[] hDiaChi = {"Địa chỉ", "Số lượng"};
                vec1 = NameAndSoLuong("SELECT DiaChi, COUNT(DiaChi) FROM DocGia GROUP BY DiaChi;");

                String[][] diaChi = new String[vec1.size()][];
                for (int j = 0; j < vec1.size(); j++) {
                    diaChi[j] = vec1.get(j).split("\t");
                }

                PrintPDF print = new PrintPDF();
                JFileChooser fc = new JFileChooser();
                int retirnval = fc.showSaveDialog(this);
                if (retirnval == JFileChooser.APPROVE_OPTION) {
                    File file = fc.getSelectedFile();
                    try {
                        print.printReaderStats(file.getPath(), total, gioiTinh[0], gioiTinh[1], hDiaChi, diaChi);
                    } catch (IOException ex) {
                        Logger.getLogger(GUIThongKeBaoCao.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

                JOptionPane.showMessageDialog(null, "Thong ke thanh cong!");

            } catch (SQLException ex) {
                Logger.getLogger(GUIThongKeBaoCao.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(null, "Thong ke loi!");
            }

        } // thong ke Sach
        // file, total, []gioitinh, []nxb, []tacgia
        if (result.equals("Sách")) {
            int total = 0, sumSach = 0, i = 0;
            try {
                rs = JdbcTemplate.query("SELECT COUNT(MaSach) FROM Sach;");
                if (rs.next()) {
                    total = rs.getInt(1); // lay tong so ma sach
                    System.out.println(total);
                }

                rs = JdbcTemplate.query("SELECT SUM(SoLuong) FROM Sach;");
                if (rs.next()) {
                    sumSach = rs.getInt(1); // lay tong so sach
                    System.out.println(sumSach);
                }

                // theo NXB
                String[] hNhaXuatban = {"Nhà xuất bản", "Số lượng"};
                vec1 = NameAndSoLuong("SELECT NXB, COUNT(NXB) FROM Sach GROUP BY NXB;");

                String[][] nhaXuatBan = new String[vec1.size()][];
                for (int j = 0; j < vec1.size(); j++) {
                    nhaXuatBan[j] = vec1.get(j).split("\t");
                }

                // theo tacgia
                String[] htacGia = {"Tên tác giả", "Số lượng"};
                vec2 = NameAndSoLuong("SELECT TacGia, COUNT(TacGia) FROM Sach GROUP BY TacGia;");

                String[][] tacGia = new String[vec2.size()][];
                for (int j = 0; j < vec2.size(); j++) {
                    tacGia[j] = vec2.get(j).split("\t");
                }

                // the the loai
                String[] hTheLoai = {"Tên thể loại", "Số lượng"};
                vec3 = NameAndSoLuong("SELECT TheLoai, COUNT(TheLoai) FROM Sach GROUP BY TheLoai;");

                String[][] theLoai = new String[vec3.size()][];
                for (int j = 0; j < vec3.size(); j++) {
                    theLoai[j] = vec3.get(j).split("\t");
                }

                PrintPDF print = new PrintPDF();
                JFileChooser fc = new JFileChooser();
                int retirnval = fc.showSaveDialog(this);
                if (retirnval == JFileChooser.APPROVE_OPTION) {
                    File file = fc.getSelectedFile();
                    try {
                        print.printBookStats(file.getPath(), sumSach, total, htacGia, tacGia, hTheLoai, theLoai, hNhaXuatban, nhaXuatBan);
                    } catch (IOException ex) {
                        Logger.getLogger(GUIThongKeBaoCao.class.getName()).log(Level.SEVERE, null, ex);
                        System.out.println("In lỗi.....!");
                    }
                }

                JOptionPane.showMessageDialog(null, "Thong ke thanh cong!");
            } catch (SQLException ex) {
                Logger.getLogger(GUIThongKeBaoCao.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(null, "Thong ke !");
            }
        }

        if (result.equals("Mượn trả")) {
            int sumSachMuon = 0, sumDGMuon = 0, sumQuaHan = 0;
            // Sach muon
            try {
                rs = JdbcTemplate.query("select COUNT(ctm.MaSachMuon) from ChiTietMuon ctm, MuonTra mt where ctm.MaMuon = mt.MaMuon AND TrangThai = N'Đang mượn';");
                if (rs.next()) {
                    sumSachMuon = rs.getInt(1);
                }

                // Doc gia muon sach
                rs = JdbcTemplate.query("SELECT  count(distinct MaDocGiaMuon) FROM MuonTra where TrangThai = N'Đang mượn';");
                if (rs.next()) {
                    sumDGMuon = rs.getInt(1);
                }
                // tong so doc gia muon qua han
                rs = JdbcTemplate.query("SELECT  count(distinct MaDocGiaMuon) FROM MuonTra where TrangThai = N'Quá hạn';");
                if(rs.next()){
                    sumQuaHan = rs.getInt(1);
                }
                // danh sach doc gia muon sach
                String[] hDGMuonSach = {"Mã mượn", "Mã độc giả mượn", "Tên độc giả mượn", "Mã sách mượn", "Tên sách mượn"};
                vec1 = dsDGMuonSach();
                String[][] dGMuonSach = new String[vec1.size()][];
                for (int j = 0; j < vec1.size(); j++) {
                    dGMuonSach[j] = vec1.get(j).split("\t");
                }

                // Ba ten sach duoc muon nhieu nhat
                String[] hBaSachMuonNhieuNhat = {"Mã sách", "Tên sách", "Số lượng"};
                vec2 = baTenSachMuonnhieuNhat();
                String[][] baSachMuonNhieuNhat = new String[vec2.size()][];
                for (int j = 0; j < vec2.size(); j++) {
                    baSachMuonNhieuNhat[j] = vec2.get(j).split("\t");
                }
                
                PrintPDF print = new PrintPDF();
                JFileChooser fc = new JFileChooser();
                int retirnval = fc.showSaveDialog(this);
                if (retirnval == JFileChooser.APPROVE_OPTION) {
                    File file = fc.getSelectedFile();
                    try {
                        print.printCheckingStats(file.getPath(), sumSachMuon, sumDGMuon, sumQuaHan, hDGMuonSach, dGMuonSach, hBaSachMuonNhieuNhat, baSachMuonNhieuNhat);
                    } catch (IOException ex) {
                        Logger.getLogger(GUIThongKeBaoCao.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

                JOptionPane.showMessageDialog(null, "Thong ke thanh cong!");

            } catch (SQLException ex) {
                Logger.getLogger(GUIThongKeBaoCao.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(null, "Thong ke loi!");
            }
        }
    }//GEN-LAST:event_btnXuatBaoCaoActionPerformed

    private void btnThongKeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnThongKeActionPerformed

        String result = (String) jcbThongKe.getSelectedItem();
        ResultSet rs1 = null, rs2 = null, rs3 = null, rs4 = null, rs5 = null, rs6 = null, rs7 = null;
        if (result.equals("Độc giả")) {
            pThongKe.removeAll();
            pThongKe.add(pDocGia, BorderLayout.CENTER);
            pThongKe.updateUI();

        }
        if (result.equals("Sách")) {
            pThongKe.removeAll();
            pThongKe.add(pSach, BorderLayout.CENTER);
            pThongKe.updateUI();
        }

        if (result.equals("Mượn trả")) {
            pThongKe.removeAll();
            pThongKe.add(pMuonTra, BorderLayout.CENTER);
            pThongKe.updateUI();
        }


    }//GEN-LAST:event_btnThongKeActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnThongKe;
    private javax.swing.JButton btnXuatBaoCao;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JComboBox<String> jcbThongKe;
    private javax.swing.JPanel pThongKe;
    private javax.swing.JTextField tfDenNgay;
    private javax.swing.JTextField tfTuNgay;
    // End of variables declaration//GEN-END:variables
}
