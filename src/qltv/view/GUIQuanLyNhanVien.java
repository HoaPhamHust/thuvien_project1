/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qltv.view;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import static javax.swing.JOptionPane.OK_CANCEL_OPTION;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import qltv.common.JdbcTemplate;
import qltv.controler.CNhanVien;
import qltv.model.dto.NhanVien;
import qltv.util.ConvertDate;
import qltv.util.DatabaseConnection;
import qltv.util.PrintPDF;

// khi insert phải chuyen Date tu java -> Date sql
// Dùng ConvertDate()
public class GUIQuanLyNhanVien extends javax.swing.JPanel {

    DefaultTableModel dm;
    CNhanVien cnhanVien = new CNhanVien();
    ResultSet rs = null;
    PreparedStatement ps = null;
    SimpleDateFormat sdf = new SimpleDateFormat();
    List<NhanVien> nhanVienList = null;

    public GUIQuanLyNhanVien() {
        initComponents();
        loadDataInTable();

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        GioiTinh = new javax.swing.ButtonGroup();
        jPanel5 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        tfMaNhanVien = new javax.swing.JTextField();
        tfTenNhanVien = new javax.swing.JTextField();
        tfDiaChi = new javax.swing.JTextField();
        tfSoDienThoai = new javax.swing.JTextField();
        dcNgaySInh = new com.toedter.calendar.JDateChooser();
        jPanel8 = new javax.swing.JPanel();
        btnXoa = new javax.swing.JButton();
        btnSua = new javax.swing.JButton();
        btnThem = new javax.swing.JButton();
        btnThemTuFile = new javax.swing.JButton();
        btnRefresh = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        tfTimKiem = new javax.swing.JTextField();
        cbbTimKiem = new javax.swing.JComboBox<>();
        btnTimKiem = new javax.swing.JButton();
        btnXuatBieuMau = new javax.swing.JButton();
        btnXuatPDF = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbNhanVien = new javax.swing.JTable();

        setLayout(new javax.swing.BoxLayout(this, javax.swing.BoxLayout.PAGE_AXIS));

        jPanel5.setPreferredSize(new java.awt.Dimension(1200, 74));

        jLabel1.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/qltv/image/Apeople.png"))); // NOI18N
        jLabel1.setText("QUẢN LÝ NHÂN VIÊN");
        jPanel5.add(jLabel1);

        add(jPanel5);

        jPanel6.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 15, 1, 15));
        jPanel6.setMaximumSize(new java.awt.Dimension(32767, 333));
        jPanel6.setMinimumSize(new java.awt.Dimension(0, 333));
        jPanel6.setPreferredSize(new java.awt.Dimension(2167, 333));
        jPanel6.setLayout(new javax.swing.BoxLayout(jPanel6, javax.swing.BoxLayout.LINE_AXIS));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 2), "Thông tin độc giả")));
        jPanel2.setMaximumSize(new java.awt.Dimension(740, 333));
        jPanel2.setMinimumSize(new java.awt.Dimension(740, 333));
        jPanel2.setPreferredSize(new java.awt.Dimension(713, 333));
        jPanel2.setLayout(new java.awt.BorderLayout());

        jLabel2.setText("Mã nhân viên : ");

        jLabel3.setText("Họ và tên :");

        jLabel4.setText("Ngày sinh : ");

        jLabel5.setText("Địa chỉ : ");

        jLabel7.setText("Số điện thoại : ");

        tfMaNhanVien.setEditable(false);

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 37, Short.MAX_VALUE)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tfDiaChi, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 541, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                        .addComponent(tfTenNhanVien, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(54, 54, 54)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(tfSoDienThoai, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addGap(29, 29, 29))
            .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel7Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jLabel2)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(tfMaNhanVien, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(178, 178, 178)
                    .addComponent(dcNgaySInh, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(30, Short.MAX_VALUE)))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4)
                .addGap(53, 53, 53)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel3)
                    .addComponent(tfTenNhanVien, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel7)
                        .addComponent(tfSoDienThoai, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(31, 31, 31)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel5)
                    .addComponent(tfDiaChi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(58, Short.MAX_VALUE))
            .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel7Layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                            .addComponent(dcNgaySInh, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addContainerGap(188, Short.MAX_VALUE))
                        .addGroup(jPanel7Layout.createSequentialGroup()
                            .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel2)
                                .addComponent(tfMaNhanVien, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
        );

        jPanel2.add(jPanel7, java.awt.BorderLayout.CENTER);

        jPanel8.setBorder(javax.swing.BorderFactory.createEmptyBorder(20, 20, 20, 20));
        jPanel8.setMaximumSize(new java.awt.Dimension(32767, 100));
        jPanel8.setMinimumSize(new java.awt.Dimension(555, 100));
        jPanel8.setLayout(new java.awt.GridLayout(1, 0, 10, 0));

        btnXoa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/qltv/image/Atru.png"))); // NOI18N
        btnXoa.setText("Xóa");
        btnXoa.setMaximumSize(new java.awt.Dimension(127, 33));
        btnXoa.setMinimumSize(new java.awt.Dimension(127, 33));
        btnXoa.setPreferredSize(new java.awt.Dimension(127, 33));
        btnXoa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnXoaActionPerformed(evt);
            }
        });
        jPanel8.add(btnXoa);

        btnSua.setIcon(new javax.swing.ImageIcon(getClass().getResource("/qltv/image/Asua.png"))); // NOI18N
        btnSua.setText("Sửa");
        btnSua.setMaximumSize(new java.awt.Dimension(127, 33));
        btnSua.setMinimumSize(new java.awt.Dimension(127, 33));
        btnSua.setPreferredSize(new java.awt.Dimension(127, 33));
        btnSua.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSuaActionPerformed(evt);
            }
        });
        jPanel8.add(btnSua);

        btnThem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/qltv/image/Add-icon.png"))); // NOI18N
        btnThem.setText("Thêm");
        btnThem.setMaximumSize(new java.awt.Dimension(127, 33));
        btnThem.setMinimumSize(new java.awt.Dimension(127, 33));
        btnThem.setPreferredSize(new java.awt.Dimension(127, 33));
        btnThem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnThemActionPerformed(evt);
            }
        });
        jPanel8.add(btnThem);

        btnThemTuFile.setIcon(new javax.swing.ImageIcon(getClass().getResource("/qltv/image/Files-icon.png"))); // NOI18N
        btnThemTuFile.setText("Thêm file");
        btnThemTuFile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnThemTuFileActionPerformed(evt);
            }
        });
        jPanel8.add(btnThemTuFile);

        btnRefresh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/qltv/image/Alammoi.png"))); // NOI18N
        btnRefresh.setText("Làm mới");
        btnRefresh.setMaximumSize(new java.awt.Dimension(127, 33));
        btnRefresh.setMinimumSize(new java.awt.Dimension(127, 33));
        btnRefresh.setPreferredSize(new java.awt.Dimension(127, 33));
        btnRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshActionPerformed(evt);
            }
        });
        jPanel8.add(btnRefresh);

        jPanel2.add(jPanel8, java.awt.BorderLayout.SOUTH);

        jPanel6.add(jPanel2);

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 2), "Tìm kiếm độc giả"));

        jLabel9.setText("Nhập từ khóa tìm kiếm : ");

        cbbTimKiem.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Mã nhân viên", "Tên nhân viên", "Địa chỉ", "Số điện thoại", "None", "", "" }));
        cbbTimKiem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbbTimKiemActionPerformed(evt);
            }
        });

        btnTimKiem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/qltv/image/Atim.png"))); // NOI18N
        btnTimKiem.setText("Tìm kiếm");
        btnTimKiem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTimKiemActionPerformed(evt);
            }
        });

        btnXuatBieuMau.setIcon(new javax.swing.ImageIcon(getClass().getResource("/qltv/image/Athongke.png"))); // NOI18N
        btnXuatBieuMau.setText("Xuất biểu mẫu");
        btnXuatBieuMau.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnXuatBieuMauActionPerformed(evt);
            }
        });

        btnXuatPDF.setIcon(new javax.swing.ImageIcon(getClass().getResource("/qltv/image/Apdf.png"))); // NOI18N
        btnXuatPDF.setText("Xuất PDF");
        btnXuatPDF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnXuatPDFActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(tfTimKiem)
                        .addGap(18, 18, 18)
                        .addComponent(cbbTimKiem, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(39, 39, 39))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel9)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnTimKiem)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnXuatBieuMau)
                .addContainerGap())
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(91, 91, 91)
                .addComponent(btnXuatPDF, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel3Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnTimKiem, btnXuatBieuMau, btnXuatPDF});

        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel9)
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfTimKiem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbbTimKiem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(32, 32, 32)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnTimKiem)
                    .addComponent(btnXuatBieuMau))
                .addGap(18, 18, 18)
                .addComponent(btnXuatPDF)
                .addContainerGap(107, Short.MAX_VALUE))
        );

        jPanel6.add(jPanel3);

        add(jPanel6);

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("Danh sách độc giả"));

        jScrollPane1.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jScrollPane1MouseClicked(evt);
            }
        });

        tbNhanVien.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tbNhanVien.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbNhanVienMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tbNhanVien);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 2125, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 354, Short.MAX_VALUE)
                .addContainerGap())
        );

        add(jPanel4);
        jPanel4.getAccessibleContext().setAccessibleName("Danh sách nhân viên");
    }// </editor-fold>//GEN-END:initComponents

    private void loadDataInTable() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        dm = new DefaultTableModel();
        dm.addColumn("Mã nhân viên");
        dm.addColumn("Tên nhân viên");
        dm.addColumn("Ngày sinh");
        dm.addColumn("Địa chỉ");
        dm.addColumn("Số điện thoại");

        // lay ra toan bo danh sach doc gia
        cnhanVien = new CNhanVien();

        nhanVienList = cnhanVien.selectAll();

        // add toan bo DL tu CSDL vao table
        for (NhanVien dg : nhanVienList) {
            Vector vector = new Vector();

            vector.add(dg.getMaNhanVien());
            vector.add(dg.getTenNhanVien());
            vector.add(dg.getNgaySinh() != null ? sdf.format(dg.getNgaySinh()) : "");
            vector.add(dg.getDiaChi() != null ? dg.getDiaChi() : "");
            vector.add(dg.getSoDienThoai() != null ? dg.getSoDienThoai() : "");
            // tao hang
            dm.addRow(vector);
        }
        // Dua DL tu dm vao table
        tbNhanVien.setModel(dm);
    }

    private void loadDateSearch() {
        SimpleDateFormat sdf = new SimpleDateFormat();

        //khởi tạo lại model, xóa hết cột cũ dòng cũ đi
        dm = new DefaultTableModel();
        //tạo cột
        dm.addColumn("Mã nhân viên");
        dm.addColumn("Tên nhân viên");
        dm.addColumn("Ngày sinh");
        dm.addColumn("Địa chỉ");
        dm.addColumn("Số điện thoại");

        // lay ra toan bo danh sach doc gia
        cnhanVien = new CNhanVien();

        NhanVien nhanVien = new NhanVien();
        nhanVien.setMaNhanVien(tfTimKiem.getText());
        nhanVien.setTenNhanVien(tfTimKiem.getText());
        nhanVien.setDiaChi(tfTimKiem.getText());
        nhanVien.setSoDienThoai(tfTimKiem.getText());

        String result = (String) cbbTimKiem.getSelectedItem();
        System.out.println("result search = " + result);

        if (result.equals("Mã nhân viên")) {
            try {
                nhanVienList = cnhanVien.search(nhanVien, result);
            } catch (SQLException ex) {
                Logger.getLogger(GUIQuanLyNhanVien.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (result.equals("Tên nhân viên")) {
            try {
                nhanVienList = cnhanVien.search(nhanVien, result);
            } catch (SQLException ex) {
                Logger.getLogger(GUIQuanLyNhanVien.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (result.equals("Địa chỉ")) {
            try {
                nhanVienList = cnhanVien.search(nhanVien, result);
            } catch (SQLException ex) {
                Logger.getLogger(GUIQuanLyNhanVien.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (result.equals("Số điện thoại")) {
            try {
                nhanVienList = cnhanVien.search(nhanVien, result);
            } catch (SQLException ex) {
                Logger.getLogger(GUIQuanLyNhanVien.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (result.equals("None")) {
            try {
                nhanVienList = cnhanVien.search(nhanVien, result);
            } catch (SQLException ex) {
                Logger.getLogger(GUIQuanLyNhanVien.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        // add toan bo DL tu CSDL vao table
        for (NhanVien dg : nhanVienList) {
            Vector vector = new Vector();

            vector.add(dg.getMaNhanVien());
            vector.add(dg.getTenNhanVien());
            vector.add(dg.getNgaySinh() != null ? sdf.format(dg.getNgaySinh()) : "");
            vector.add(dg.getDiaChi() != null ? dg.getDiaChi() : "");
            vector.add(dg.getSoDienThoai() != null ? dg.getSoDienThoai() : "");
            // tao hang
            dm.addRow(vector);
        }
        // Dua DL tu dm vao table
        tbNhanVien.setModel(dm);

    }

    private void clear() {
        tfMaNhanVien.setText("");
        tfTenNhanVien.setText("");
        tfDiaChi.setText("");
        tfSoDienThoai.setText("");
    }
// sai

    // 
    private void themFile() {
        JFileChooser chooser = new JFileChooser();
        chooser.setCurrentDirectory(new File("G:\\"));
        int check = chooser.showOpenDialog(this);
        if (check == chooser.APPROVE_OPTION) {

            try {
                String[] titile = new String[]{"Mã nhân viên", "Tên nhân viên", "Ngày sinh", "Địa chỉ", "Số điện thoại"};

                dm = new DefaultTableModel(titile, 0);
                File file = chooser.getSelectedFile();

                Workbook wb = Workbook.getWorkbook(file);
                Sheet sheet = wb.getSheet(0);

                Cell cell;

                //List<NhanVien> nhanVienList = null;
//                DatabaseConnection conn = new DatabaseConnection();
                String[] values = new String[5];
                for (int i = 1; i < sheet.getRows(); i++) {
                    NhanVien dg = new NhanVien();

                    cell = sheet.getCell(0, i);
                    //values[0] = cell.getContents();
                    dg.setMaNhanVien(cell.getContents());

                    // ps.setDate(4, ConvertDate.convertDate(nhanVien.getNgaySinh()));
                    cell = sheet.getCell(1, i);
                    //values[1] = cell.getContents();
                    dg.setTenNhanVien(cell.getContents());
                    
                    sdf = new SimpleDateFormat("MM/dd/yyyy");
                    cell = sheet.getCell(2, i);
                    //values[3] = cell.getContents();
                    dg.setNgaySinh(ConvertDate.convertDate(sdf.parse(cell.getContents()))); // convert String -> Date in java
                    cell = sheet.getCell(3, i);
                    //values[4] = cell.getContents();
                    dg.setDiaChi(cell.getContents());
                    cell = sheet.getCell(4, i);
                    //values[5] = cell.getContents();
                    dg.setSoDienThoai(cell.getContents());

                    nhanVienList.add(dg);
                    cnhanVien.insert(dg); // insert DL vao trong CSDL
                    //conn.insert("NhanVien", values); // insert DL vao trong CSDL
                    //dm.addRow(values);
                }
                Vector tenCot = new Vector();
                tenCot.add("Cot1");
                tenCot.add("Cot1");
                tenCot.add("Cot1");
                tenCot.add("Cot1");
                tenCot.add("Cot1");

                Vector v = new Vector(nhanVienList);
                dm = new DefaultTableModel(v, tenCot);
                tbNhanVien.setModel(dm);
            } catch (IOException ex) {
                Logger.getLogger(GUIQuanLyNhanVien.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(null, "Lỗi IOException");
            } catch (BiffException ex) {
                Logger.getLogger(GUIQuanLyNhanVien.class.getName()).log(Level.SEVERE, null, ex);

            } catch (ParseException ex) {
                Logger.getLogger(GUIQuanLyNhanVien.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void themFile1() {
        JFileChooser chooser = new JFileChooser();
        chooser.setCurrentDirectory(new File("G:\\"));
        int check = chooser.showOpenDialog(this);
        if (check == chooser.APPROVE_OPTION) {

            try {
                String[] titile = new String[]{"Mã nhân viên", "Tên nhân viên", "Ngày sinh", "Địa chỉ", "Số điện thoại"};

                dm = new DefaultTableModel(titile, 0);
                File file = chooser.getSelectedFile();

                Workbook wb = Workbook.getWorkbook(file);
                Sheet sheet = wb.getSheet(0);

                Cell cell;

                //List<NhanVien> nhanVienList = null;
                DatabaseConnection conn = DatabaseConnection.getInstance();
                String[] values = new String[5];
                for (int i = 1; i < sheet.getRows(); i++) {
                    NhanVien dg = new NhanVien();

                    cell = sheet.getCell(0, i);
                    values[0] = cell.getContents();
                    //dg.setMaNhanVien(cell.getContents());

                    cell = sheet.getCell(1, i);
                    values[1] = cell.getContents();
                    //dg.setTenNhanVien(cell.getContents());
                    cell = sheet.getCell(2, i);
                    values[2] = cell.getContents();
                    //sdf = new SimpleDateFormat("MM/dd/yyyy");
                    //dg.setHanSuDung(ConvertDate.convertDate(sdf.parse(cell.getContents())));
                    cell = sheet.getCell(3, i);
                    values[3] = cell.getContents();
                    //dg.setNgaySinh(ConvertDate.convertDate(sdf.parse(cell.getContents()))); // convert String -> Date in java
                    cell = sheet.getCell(4, i);
                    values[4] = cell.getContents();
                    //dg.setDiaChi(cell.getContents());


//                   nhanVienList.add(dg);
//                   cnhanVien.insert(dg); // insert DL vao trong CSDL
                    conn.insert("NhanVien", values); // insert DL vao trong CSDL
                    dm.addRow(values);
                }
//                Vector tenCot = new Vector();
//                tenCot.add("Cot1");
//                tenCot.add("Cot1");
//                tenCot.add("Cot1");tenCot.add("Cot1");
//                tenCot.add("Cot1");
//                tenCot.add("Cot1");
//                tenCot.add("Cot1");
//                
//                Vector v = new Vector(nhanVienList);
//                dm = new DefaultTableModel(v,tenCot);
                tbNhanVien.setModel(dm);
            } catch (IOException ex) {
                Logger.getLogger(GUIQuanLyNhanVien.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(null, "Lỗi IOException");
            } catch (BiffException ex) {
                Logger.getLogger(GUIQuanLyNhanVien.class.getName()).log(Level.SEVERE, null, ex);

            }
        }
    }

    private void writeFileExcel() {

        WritableWorkbook wb;
        JFileChooser chooser = new JFileChooser();
        chooser.setCurrentDirectory(new File("G:\\"));
        int check = chooser.showSaveDialog(this);
        if (check == chooser.APPROVE_OPTION) {

            try {
                File file = chooser.getSelectedFile();
                wb = Workbook.createWorkbook(file);
                WritableSheet sheet = wb.createSheet("NhanVien", 0);
                try {
                    sheet.addCell(new jxl.write.Label(0, 0, "MaNV")); // cột 0 d
                    sheet.addCell(new jxl.write.Label(1, 0, "TenDG"));
                    sheet.addCell(new jxl.write.Label(2, 0, "HanSuDung"));
                    sheet.addCell(new jxl.write.Label(3, 0, "NgaySinh"));
                    sheet.addCell(new jxl.write.Label(4, 0, "DiaChi"));
                    sheet.addCell(new jxl.write.Label(5, 0, "SoDienThoai"));
                    sheet.addCell(new jxl.write.Label(6, 0, "GioiTinh"));
                    int rowBegin = 1;
                    TableModel tableModel = tbNhanVien.getModel();
                    for (int row = rowBegin, i = 0; row < rowBegin + tbNhanVien.getRowCount(); row++, i++) {
                        sheet.addCell(new jxl.write.Label(0, row, (String) tableModel.getValueAt(i, 0)));
                        sheet.addCell(new jxl.write.Label(1, row, (String) tableModel.getValueAt(i, 1)));
                        sheet.addCell(new jxl.write.Label(2, row, (String) tableModel.getValueAt(i, 2)));
                        sheet.addCell(new jxl.write.Label(3, row, (String) tableModel.getValueAt(i, 3)));
                        sheet.addCell(new jxl.write.Label(4, row, (String) tableModel.getValueAt(i, 4)));
                        sheet.addCell(new jxl.write.Label(5, row, (String) tableModel.getValueAt(i, 5)));
                        sheet.addCell(new jxl.write.Label(6, row, (String) tableModel.getValueAt(i, 6)));

                    }
                    wb.write();
                    wb.close();
                    JOptionPane.showMessageDialog(null, "success!");
                } catch (WriteException ex) {
                    Logger.getLogger(GUIQuanLyNhanVien.class.getName()).log(Level.SEVERE, null, ex);
                }

            } catch (IOException ex) {
                Logger.getLogger(GUIQuanLyNhanVien.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    private void btnRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshActionPerformed
        loadDataInTable();
    }//GEN-LAST:event_btnRefreshActionPerformed

    private void btnThemTuFileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnThemTuFileActionPerformed
        themFile1();
    }//GEN-LAST:event_btnThemTuFileActionPerformed

    private void btnTimKiemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTimKiemActionPerformed
        loadDateSearch();
    }//GEN-LAST:event_btnTimKiemActionPerformed

    private void btnXuatBieuMauActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnXuatBieuMauActionPerformed
        writeFileExcel();
    }//GEN-LAST:event_btnXuatBieuMauActionPerformed

    private void btnXuatPDFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnXuatPDFActionPerformed
        String[] header = {"Mã nhân viên", "Tên nhân viên", "Ngày sinh", "Địa chỉ", "Số điện thoại"};

        loadDateSearch();
        String[][] data = new String[nhanVienList.size()][];
        String str = null;
        for (int i = 0; i < nhanVienList.size(); i++) {
            str = nhanVienList.get(i).getMaNhanVien() + "\t" + nhanVienList.get(i).getTenNhanVien() + "\t" + nhanVienList.get(i).getNgaySinh() + "\t" + nhanVienList.get(i).getDiaChi() + "\t" + nhanVienList.get(i).getSoDienThoai();
            data[i] = str.split("\t");
        }

        PrintPDF print = new PrintPDF();
        JFileChooser fc = new JFileChooser();
        int retirnval = fc.showSaveDialog(this);
        if (retirnval == JFileChooser.APPROVE_OPTION) {
            try {
                File file = fc.getSelectedFile();
                print.printSearchReport(file.getPath(), "NhanVien", header, data, (String) cbbTimKiem.getSelectedItem(), tfTimKiem.getText(), "nhân viên");
            } catch (IOException ex) {
                Logger.getLogger(GUIQuanLyNhanVien.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_btnXuatPDFActionPerformed

    private void tbNhanVienMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbNhanVienMouseClicked
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

            // hien thi du lieu cua hang da chon trong jTabel
            int row = tbNhanVien.getSelectedRow();
            tfMaNhanVien.setText((String) tbNhanVien.getValueAt(row, 0));
            tfTenNhanVien.setText((String) tbNhanVien.getValueAt(row, 1));

            String ngaySinh = (String) tbNhanVien.getValueAt(row, 2);
            dcNgaySInh.setDate(ngaySinh != null && (!ngaySinh.equals("")) ? sdf.parse(ngaySinh) : null);

            String diaChi = (String) tbNhanVien.getValueAt(row, 3);
            tfDiaChi.setText(diaChi != null ? diaChi : null);

            String soDienThoai = (String) tbNhanVien.getValueAt(row, 4);
            tfSoDienThoai.setText(soDienThoai != null ? soDienThoai : null);

        } catch (ParseException ex) {
            Logger.getLogger(GUIQuanLyNhanVien.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_tbNhanVienMouseClicked

    private void jScrollPane1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jScrollPane1MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jScrollPane1MouseClicked

    private void btnSuaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSuaActionPerformed
        int result = JOptionPane.showConfirmDialog(this, "Bạn muốn thêm dữ liệu ?", "Thêm nhân viên", JOptionPane.YES_NO_OPTION);
        if (result == JOptionPane.YES_OPTION) {

            if (tfMaNhanVien.getText().isEmpty()) {
                JOptionPane.showMessageDialog(this, "Chưa nhập mã nhân viên");
                return;
            }
            if (tfTenNhanVien.getText().isEmpty()) {
                JOptionPane.showMessageDialog(this, "Chưa nhập tên nhân viên");
                return;
            }

            NhanVien nhanVien = new NhanVien();
            nhanVien.setMaNhanVien(tfMaNhanVien.getText());
            nhanVien.setTenNhanVien(tfTenNhanVien.getText());
            nhanVien.setDiaChi(tfDiaChi.getText());
            nhanVien.setSoDienThoai(tfSoDienThoai.getText());
            nhanVien.setNgaySinh(dcNgaySInh.getDate());

            boolean upd = cnhanVien.update(nhanVien);
            if (upd) {
                JOptionPane.showMessageDialog(this, "Sửa nhân viên thành công!");
            }

            loadDataInTable();
        }

    }//GEN-LAST:event_btnSuaActionPerformed

    private void btnThemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnThemActionPerformed
        int result = JOptionPane.showConfirmDialog(this, "Bạn muốn thêm dữ liệu ?", "Thêm nhân viên", JOptionPane.YES_NO_OPTION);
        if (result == JOptionPane.YES_OPTION) {
            if (tfTenNhanVien.getText().isEmpty()) {
                JOptionPane.showMessageDialog(this, "Chưa nhập tên nhân viên");
                return;
            }

            String sql = "SELECT * FROM NhanVien WHERE MaNV = '" + tfMaNhanVien.getText() + "'";
            try {
                rs = JdbcTemplate.query(sql);
            } catch (SQLException ex) {
                Logger.getLogger(GUIQuanLyNhanVien.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                if (!rs.next()) {

                    NhanVien nhanVien = new NhanVien();
                    nhanVien.setMaNhanVien(tfMaNhanVien.getText());
                    nhanVien.setTenNhanVien(tfTenNhanVien.getText());
                    nhanVien.setDiaChi(tfDiaChi.getText());
                    nhanVien.setSoDienThoai(tfSoDienThoai.getText());
                    nhanVien.setNgaySinh(dcNgaySInh.getDate());

                    boolean upd = cnhanVien.insert(nhanVien);
                    if (upd) {
                        JOptionPane.showMessageDialog(this, "Thêm nhân viên thành công!");
                    }

                    loadDataInTable();
                } else {
                    JOptionPane.showMessageDialog(null, "Mã nhân viên đã tồn tại!");
                }
            } catch (SQLException ex) {
                Logger.getLogger(GUIQuanLyNhanVien.class.getName()).log(Level.SEVERE, null, ex);

            }
        }

    }//GEN-LAST:event_btnThemActionPerformed

    private void btnXoaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnXoaActionPerformed
        //Hiển thị confirm
        int confirm = JOptionPane.showConfirmDialog(this, "Bạn có muốn xóa không?", "Thông báo", OK_CANCEL_OPTION);
        //Nếu chọn Yes
        String sql1 = "SELECT MaNhanVienMuon FROM MuonTra WHERE MaNhanVienMuon = '" + tfMaNhanVien.getText().trim() + "'";
        if (confirm == JOptionPane.YES_OPTION) {
            try {
                rs = JdbcTemplate.query(sql1);
                if (rs.next()) {
                    JOptionPane.showMessageDialog(null, "Doc gia dang muon sach! ");

                } else {
                    NhanVien nhanVien = new NhanVien(tfMaNhanVien.getText());
                    boolean result = cnhanVien.delete(nhanVien);
                    if (result == true) {
                        JOptionPane.showMessageDialog(this, "Xóa nhân viên thành công!");
                    }
                }
                clear();
                loadDataInTable();

            } catch (SQLException ex) {
                Logger.getLogger(GUIQuanLyNhanVien.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_btnXoaActionPerformed

    private void cbbTimKiemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbbTimKiemActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbbTimKiemActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup GioiTinh;
    private javax.swing.JButton btnRefresh;
    private javax.swing.JButton btnSua;
    private javax.swing.JButton btnThem;
    private javax.swing.JButton btnThemTuFile;
    private javax.swing.JButton btnTimKiem;
    private javax.swing.JButton btnXoa;
    private javax.swing.JButton btnXuatBieuMau;
    private javax.swing.JButton btnXuatPDF;
    private javax.swing.JComboBox<String> cbbTimKiem;
    private com.toedter.calendar.JDateChooser dcNgaySInh;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tbNhanVien;
    private javax.swing.JTextField tfDiaChi;
    private javax.swing.JTextField tfMaNhanVien;
    private javax.swing.JTextField tfSoDienThoai;
    private javax.swing.JTextField tfTenNhanVien;
    private javax.swing.JTextField tfTimKiem;
    // End of variables declaration//GEN-END:variables
}
