/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qltv.view;

import com.marchboy.combobox.ComboBoxFilterDecorator;
import com.marchboy.combobox.ObjectShowing;
import java.io.File;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import net.proteanit.sql.DbUtils;
import qltv.common.JdbcTemplate;
import qltv.controler.CMuonTra;
import qltv.model.dto.MuonTra;
import qltv.util.DatabaseConnection;
import qltv.util.PrintPDF;
import qltv.controler.CDocGia;
import qltv.controler.CNhanVien;
import qltv.controler.CSach;
import qltv.model.dao.DaoFactory;
import qltv.model.dto.DocGia;
import qltv.model.dto.NhanVien;
import qltv.model.dto.Sach;

/**
 *
 * @author dell
 */
public class GUIQuanLyMuonTra extends javax.swing.JPanel {

    Vector<String> vec;
    DefaultTableModel dm = null;
    ResultSet rs = null;
    int row = 0; // dong duoc chon trông bang
    private String maMuon;
    CMuonTra cMuonTra = null;
    DatabaseConnection conn;
    List<Sach> availabelBooks;
    List<DocGia> listDocGia;
    List<NhanVien> listNhanVien;

    DocGia docGia;
    NhanVien nhanVien;
    Sach sach;
    Sach selectedSach;
    MuonTra muonTra;

    NhanVien modelNhanVien[];
    DocGia modelDocGia[];
    Sach modelSach[];

    Vector<String> maSachMuon;

    public GUIQuanLyMuonTra() {
        initComponents();
        conn = DatabaseConnection.getInstance();
        pChiTietMuonTra.setVisible(false);
        updateBangMuon();
        updateChiTietMuon();
        updateChiTietPhieuMuon();
        setUpComboBox();
    }

    void setUpComboBox() {
        this.availabelBooks = new CSach().selectAll();
        this.listDocGia = new CDocGia().selectAll();
        this.listNhanVien = new CNhanVien().selectAll();

        this.modelDocGia = listDocGia.toArray(new DocGia[listDocGia.size()]);
        this.modelNhanVien = listNhanVien.toArray(new NhanVien[listNhanVien.size()]);
        this.modelSach = availabelBooks.toArray(new Sach[availabelBooks.size()]);

        cbDocgia.setModel(new DefaultComboBoxModel<>(modelDocGia));
        cbNhanVien.setModel(new DefaultComboBoxModel<>(modelNhanVien));
        cbSach.setModel(new DefaultComboBoxModel<>(modelSach));

        ComboBoxFilterDecorator.decorate(cbDocgia, new ObjectShowing<DocGia>() {
            @Override
            public String getPreferredStringForItem(DocGia item) {
                return item.getMaDocGia();
            }
        });

        ComboBoxFilterDecorator.decorate(cbNhanVien, new ObjectShowing<NhanVien>() {
            @Override
            public String getPreferredStringForItem(NhanVien item) {
                return item.getMaNhanVien();
            }
        });

        ComboBoxFilterDecorator.decorate(cbSach, new ObjectShowing<Sach>() {
            @Override
            public String getPreferredStringForItem(Sach item) {
                return String.format("%s [%s]", item.getMaSach(), item.getTenSach());
            }
        });

//        DatabaseConnection.getInstance().updateTable(tbMuonTra, "SELECT * FROM ChiTietMuon");
        setUpMuonTraTable();
        clear();
    }

    private void clear() {
        lbDocGia.setText("");
        lbNhanVien.setText("");
        lbSoLuongConLai.setText("");
        lbNgayMuon.setText("");
        lbHanTra.setText("");
        pChiTietMuonTra.setVisible(false);
        muonTra = null;
        lbMaMuon.setText("Mã Mượn:");
        btnMaMuon.setText("Lấy Mã Mượn");
        btnXoaSach.setEnabled(false);
        btnThemSach.setEnabled(false);
        DefaultTableModel model = (DefaultTableModel) tbMuonTra.getModel();
        model.setRowCount(0);
        clearSachPanel();
    }
    
    private void clearSachPanel() {
        btnXoaSach.setEnabled(false);
        btnThemSach.setEnabled(false);
        lbSoLuongConLai.setText("");
        sach = null;
        
    }

    private void btnMaMuonClicked() {
        if (docGia == null || nhanVien == null) {
            JOptionPane.showMessageDialog(this, "Chọn mã độc giả và mã nhân viên");
            return;
        }
        if (muonTra != null) {
            int i = JOptionPane.showConfirmDialog(this, "Bạn có muốn huỷ mượn trả?", "Cảnh báo", JOptionPane.YES_NO_OPTION);
            if (i == JOptionPane.YES_OPTION) {
                int t = DaoFactory.getDao("MuonTra").delete(muonTra);
                clear();
                System.out.println(t);
            }
            return;
        } else {
            SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
            String[] values = new String[5];
            values[0] = docGia.getMaDocGia();
            values[1] = nhanVien.getMaNhanVien();
            values[2] = sdf.format(new Date());
            Date dt = new Date();
            Calendar c = Calendar.getInstance();
            c.setTime(dt);
            c.add(Calendar.MONTH, 2);
            dt = c.getTime();
            values[3] = sdf.format(dt);
            values[4] = "Nợ Sách";

            int m = DatabaseConnection.getInstance().insert("MuonTra", values);
            if (m > 0) {
                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                muonTra = (MuonTra) DaoFactory.getDao("MuonTra").findById(m);
                maMuon = muonTra.getMaMuon();
                btnMaMuon.setText("Huỷ");
                lbMaMuon.setText("Mã Mượn: " + maMuon);
                pChiTietMuonTra.setVisible(true);
                maSachMuon = new Vector<>();
                lbNgayMuon.setText(format.format(muonTra.getNgayMuon()));
                lbHanTra.setText(format.format(muonTra.getHanTra()));
            }
        }
    }

    private void btnThemSach() {
        Vector<String> sachVector = new Vector<>();
        sachVector.add(sach.getMaSach());
        sachVector.add(sach.getTenSach());
        sachVector.add(sach.getTacGia());
        sachVector.add(sach.getNxb());

        if (!maSachMuon.contains(sachVector.get(0))) {
            if (sach.getSoLuong() > 0) {
                DefaultTableModel model = (DefaultTableModel) tbMuonTra.getModel();
                model.addRow(sachVector);
                maSachMuon.add(sachVector.get(0));
                sach.setSoLuong(sach.getSoLuong() - 1);
                lbSoLuongConLai.setText(String.format("Số lượng sách còn lại: %d", sach.getSoLuong()));
                DaoFactory.getDao("Sach").update(sach);
                clearSachPanel();
            } else {
                JOptionPane.showMessageDialog(this, "Sách đã được mượn hết!");

            }
        } else {
            JOptionPane.showMessageDialog(this, "Đã có sách này trong danh sách mượn");
        }
    }

    private void btnXoaSach() {
        DefaultTableModel model = (DefaultTableModel) tbMuonTra.getModel();
        Vector<String> sachVector = (Vector<String>) model.getDataVector().get(tbMuonTra.getSelectedRow());
        String code = sachVector.get(0);
        for (Sach i: availabelBooks) {
            if (i.getMaSach().equals(code)) {
                sach = i;
                break;
            }
        }
        maSachMuon.remove(code);
        model.removeRow(tbMuonTra.getSelectedRow());
        sach.setSoLuong(sach.getSoLuong() + 1);
        lbSoLuongConLai.setText(String.format("Số lượng sách còn lại: %d", sach.getSoLuong()));
        DaoFactory.getDao("Sach").update(sach);
        clearSachPanel();
        btnXoaSach.setEnabled(false);
    }
    
    private void btnMuonSach() {
        String sql = "INSERT INTO ChiTietMuon (MaMuon, MaSachMuon, NgayTra, TienPhat) VALUES (?, ?, ?, 0)";
        JdbcTemplate.batch(sql, new JdbcTemplate.BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement pstmt, int i) throws SQLException {
                pstmt.setString(1, muonTra.getMaMuon());
                pstmt.setString(2, maSachMuon.get(i));
                pstmt.setDate(3, null);
            }

            @Override
            public int getBatchSize() {
                return maSachMuon.size();
            }
        });
        clear();
    }

    private void setUpMuonTraTable() {
        DefaultTableModel model = new DefaultTableModel();
        model.addColumn("Mã Sách");
        model.addColumn("Tên Sách");
        model.addColumn("Tác giả");
        model.addColumn("Nhà xuất bản");

        tbMuonTra.setModel(model);
    }

    public void updateBangMuon() {
//        conn.updateTable(jtableMuonSach, "SELECT * FROM MuonTra ");

    }

    public void updateChiTietMuon() {
//        conn.updateTable(jtableChiTietMuonSach, "SELECT * FROM ChiTietMuon");
    }

    public void updateChiTietPhieuMuon() {
        conn.updateTable(jtablehiTietPhieuMuon, "SELECT ctm.MaMuon, ctm.MaSachMuon, s.TenSach, ctm.NgayTra, ctm.TienPhat  FROM MuonTra mt, ChiTietMuon ctm, Sach s "
                + " WHERE ctm.MaMuon = mt.MaMuon AND s.MaSach = ctm.MaSachMuon");
    }

    private void loadDLSearch() {
        try {
            String sql = "SELECT ctm.MaMuon, ctm.MaSachMuon, s.TenSach, ctm.NgayTra, ctm.TienPhat  FROM MuonTra mt, ChiTietMuon ctm, Sach s "
                + " WHERE ctm.MaMuon = mt.MaMuon AND s.MaSach = ctm.MaSachMuon AND ctm.MaMuon = '" + tfMaMuon2.getText() + "' ;";
            rs = JdbcTemplate.query(sql);

        } catch (SQLException ex) {
            Logger.getLogger(GUIQuanLyMuonTra.class.getName()).log(Level.SEVERE, null, ex);
        }

        jtablehiTietPhieuMuon.setModel(DbUtils.resultSetToTableModel(rs));
    }

    public Vector<String> PhieuMuon(String maMuon) {
        Vector<String> vector = new Vector<String>(100);
        String sql = "";
        try {

            sql = "SELECT ctm.MaMuon, ctm.MaSachMuon, s.TenSach, ctm.NgayTra, ctm.TienPhat  FROM MuonTra mt, ChiTietMuon ctm, Sach s "
                + " WHERE ctm.MaMuon = mt.MaMuon AND s.MaSach = ctm.MaSachMuon AND mt.MaMuon = '" + maMuon + "';";
            rs = JdbcTemplate.query(sql);
            while (rs.next()) {
                vector.add(rs.getString(1) + "\t" + rs.getString(2) + "\t" + rs.getString(3) + "\t" + rs.getDate(4) + "\t" + rs.getFloat(5));
            }

        } catch (SQLException ex) {
            Logger.getLogger(GUIQuanLyMuonTra.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Loi loi loiiiii" + ex.toString());
        }
        return vector;
    }
    
        private void btnTraSach() {
//        if (tfMaMuon1.getText().equals("")) {
//            JOptionPane.showMessageDialog(null, "Ma muon khong duoc bo trong!");
//        }
//        if (tfMaDocGia4.getText().equals("")) {
//            JOptionPane.showMessageDialog(null, "Ma doc gia khong duoc bo trong!");
//        } else {
//            try {
//
//                String time = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
//
//                rs = JdbcTemplate.query("SELECT * FROM MuonTra WHERE MaMuon = '" + tfMaMuon1.getText() + "';");
//
//                int comp = 0;
//                System.out.println("tome = " + time);
//
//                while (rs.next()) {
//                    System.out.println("rs.getString(5) = " + rs.getString(5));
//                    comp = time.compareTo(rs.getString(5)); // so sanh ngay hien tai voi han tra
//
//                }
//
//                if (comp >= 0) {
//                    JOptionPane.showMessageDialog(null, "Tra sach qua han!");
//
//                    String sql2 = "UPDATE Sach SET SoLuong = SoLuong + 1 WHERE MaSach in (SELECT MaSach FROm Sach s, ChiTietMuon ctm, MuonTra mt "
//                            + "WHERE mt.MaMuon = ctm.MaMuon AND s.MaSach = ctm.MaSachMuon  "
//                            + "AND mt.MaMuon =  " + tfMaMuon1.getText() + ");";
//                    System.out.println(sql2);
//                    JdbcTemplate.query(sql2);
//
//                    String sql3 = "UPDATE MuonTra SET NgayTra = GETDATE(), TrangThai = N'Quá hạn' Where MaMuon = " + tfMaMuon1.getText();
//                    JdbcTemplate.query(sql3);
//
//                    String sql1 = "update MuonTra set TienPhat =  DATEDIFF(DAY,HanTra, NgayTra)*3000 WHERE MaMuon = " + tfMaMuon1.getText();
//                    JdbcTemplate.query(sql1);
//
//                    updateChiTietMuon();
//                    updateBangMuon();
//
//                } else {
//                    JOptionPane.showMessageDialog(null, "Tra sach thanh cong!");
//                    String sql2 = "UPDATE Sach SET SoLuong = SoLuong + 1 WHERE MaSach in (SELECT MaSach FROm Sach s, ChiTietMuon ctm, MuonTra mt "
//                            + "WHERE mt.MaMuon = ctm.MaMuon AND s.MaSach = ctm.MaSachMuon  "
//                            + "AND mt.MaMuon =  " + tfMaMuon1.getText() + ");";
//                    System.out.println(sql2);
//                    JdbcTemplate.query(sql2);
//
//                    String sql3 = "UPDATE  MuonTra SET NgayTra = GETDATE(), TrangThai = N'Đã trả' Where MaMuon = " + tfMaMuon1.getText();
//                    JdbcTemplate.query(sql3);
//                    //
//                    updateChiTietMuon();
//                    updateBangMuon();
//
//                }
//
//            } catch (SQLException ex) {
//                Logger.getLogger(GUIQuanLyMuonTra.class.getName()).log(Level.SEVERE, null, ex);
//            }
//
//        }
    }
    
    private void xoaSach() {
        MuonTra mt = new MuonTra(maMuon);
        //mt.setMaMuon(maMuon);
        cMuonTra = new CMuonTra();
        boolean result1 = cMuonTra.deleteChiTietMuon(mt);
        boolean result2 = false;

        if (result1 = true) {
            result2 = cMuonTra.deleteMuonTra(mt);
            if (result2 = true) {
                JOptionPane.showMessageDialog(null, "Xóa thành công!");
                updateBangMuon();
                updateChiTietMuon();
                updateChiTietPhieuMuon();

            }

        } else {
            JOptionPane.showMessageDialog(null, "Chưa xóa được mã mượn!");
        }
    }
    
    private void cbDocGiaClicked() {
        docGia = (DocGia) cbDocgia.getSelectedItem();
        lbDocGia.setText(docGia.getTenDocGia());
    }
    
    void cbNhanVienClicked() {
        nhanVien = (NhanVien) cbNhanVien.getSelectedItem();
        lbNhanVien.setText(nhanVien.getTenNhanVien());
    }
    
    void cbSachClicked() {
        sach = (Sach) cbSach.getSelectedItem();
        btnThemSach.setEnabled(true);
        int soluong = sach.getSoLuong();
        String text;
        if (soluong <= 0) {
            text = "Số lượng sách còn lại: 0";
            btnThemSach.setEnabled(false);
        } else {
            text = String.format("Số lượng sách còn lại: %d", soluong);
            btnThemSach.setEnabled(true);
        }
        lbSoLuongConLai.setText(text);
    }
    
    void btnMuonTraClicked() {
        btnXoaSach.setEnabled(true);
    }
    
    void inPhieuMuon() {
        String[] header = {"Mã mượn", "Mã sách mượn", "Tên sách", "Ngày trả", "Tiền phạt"};

        vec = PhieuMuon(tfMaMuon2.getText());
        // (Integer.parseInt(tfMaMuon2.getText())
        String[][] data = new String[vec.size()][];
        for (int i = 0; i < vec.size(); i++) {
            data[i] = vec.get(i).split("\t");
        }
        PrintPDF print = new PrintPDF();
        JFileChooser fc = new JFileChooser();
        int retirnval = fc.showSaveDialog(this);
        if (retirnval == JFileChooser.APPROVE_OPTION) {
            try {
                File file = fc.getSelectedFile();
                print.printChiTietPhieuMuon(file.getPath(), header, data);
            } catch (IOException ex) {
                Logger.getLogger(GUIQuanLyMuonTra.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel22 = new javax.swing.JPanel();
        jDesktopPane1 = new javax.swing.JDesktopPane();
        jButton1 = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        panelInfo = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        cbDocgia = new javax.swing.JComboBox<>();
        jLabel5 = new javax.swing.JLabel();
        lbDocGia = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        cbNhanVien = new javax.swing.JComboBox<>();
        jLabel7 = new javax.swing.JLabel();
        lbNhanVien = new javax.swing.JLabel();
        jPanel11 = new javax.swing.JPanel();
        btnMaMuon = new javax.swing.JButton();
        lbMaMuon = new javax.swing.JLabel();
        pChiTietMuonTra = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        cbSach = new javax.swing.JComboBox<>();
        btnThemSach = new javax.swing.JButton();
        lbSoLuongConLai = new javax.swing.JLabel();
        btnXoaSach = new javax.swing.JButton();
        btnMuonSach = new javax.swing.JButton();
        jPanel8 = new javax.swing.JPanel();
        jPanel12 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        lbNgayMuon = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        lbHanTra = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbMuonTra = new javax.swing.JTable();
        jPanel18 = new javax.swing.JPanel();
        jPanel20 = new javax.swing.JPanel();
        jPanel10 = new javax.swing.JPanel();
        jLabel15 = new javax.swing.JLabel();
        tfMaMuon2 = new javax.swing.JTextField();
        btnTimKiem5 = new javax.swing.JButton();
        jPanel13 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        lbTenDocGia = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        lbTenNhanVien = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        lbNgayMuonPhieu = new javax.swing.JLabel();
        lbHanTraPhieu = new javax.swing.JLabel();
        jPanel21 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jtablehiTietPhieuMuon = new javax.swing.JTable();
        jPanel9 = new javax.swing.JPanel();
        btnRefresh3 = new javax.swing.JButton();
        btnInPhieu = new javax.swing.JButton();

        javax.swing.GroupLayout jPanel22Layout = new javax.swing.GroupLayout(jPanel22);
        jPanel22.setLayout(jPanel22Layout);
        jPanel22Layout.setHorizontalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel22Layout.setVerticalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jDesktopPane1Layout = new javax.swing.GroupLayout(jDesktopPane1);
        jDesktopPane1.setLayout(jDesktopPane1Layout);
        jDesktopPane1Layout.setHorizontalGroup(
            jDesktopPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jDesktopPane1Layout.setVerticalGroup(
            jDesktopPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        jButton1.setText("jButton1");

        setMaximumSize(new java.awt.Dimension(1028, 490));
        setMinimumSize(new java.awt.Dimension(1028, 490));
        setName(""); // NOI18N
        setPreferredSize(new java.awt.Dimension(1028, 490));
        setLayout(new javax.swing.BoxLayout(this, javax.swing.BoxLayout.Y_AXIS));

        jPanel4.setMaximumSize(new java.awt.Dimension(1020, 64));
        jPanel4.setMinimumSize(new java.awt.Dimension(1020, 64));

        jLabel1.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/qltv/image/Apaper.png"))); // NOI18N
        jLabel1.setText("QUẢN LÝ MƯỢN TRẢ");
        jPanel4.add(jLabel1);

        add(jPanel4);

        jTabbedPane1.setBorder(javax.swing.BorderFactory.createEmptyBorder(20, 20, 20, 20));
        jTabbedPane1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jTabbedPane1.addContainerListener(new java.awt.event.ContainerAdapter() {
            public void componentAdded(java.awt.event.ContainerEvent evt) {
                jTabbedPane1ComponentAdded(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.setLayout(new javax.swing.BoxLayout(jPanel1, javax.swing.BoxLayout.PAGE_AXIS));

        jPanel5.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jPanel5.setMaximumSize(new java.awt.Dimension(973, 180));
        jPanel5.setMinimumSize(new java.awt.Dimension(973, 180));
        jPanel5.setName(""); // NOI18N
        jPanel5.setPreferredSize(new java.awt.Dimension(973, 180));
        jPanel5.setLayout(new javax.swing.BoxLayout(jPanel5, javax.swing.BoxLayout.LINE_AXIS));

        panelInfo.setBorder(javax.swing.BorderFactory.createEmptyBorder(5, 5, 5, 5));
        panelInfo.setMaximumSize(new java.awt.Dimension(300, 320));
        panelInfo.setMinimumSize(new java.awt.Dimension(300, 320));
        panelInfo.setPreferredSize(new java.awt.Dimension(250, 320));
        panelInfo.setLayout(new java.awt.GridLayout(3, 1, 0, 5));

        jPanel6.setPreferredSize(new java.awt.Dimension(400, 163));
        jPanel6.setLayout(new java.awt.GridLayout(2, 2));

        jLabel2.setText("Mã độc giả");
        jPanel6.add(jLabel2);

        cbDocgia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbDocgiaActionPerformed(evt);
            }
        });
        jPanel6.add(cbDocgia);

        jLabel5.setText("Tên độc giả");
        jLabel5.setToolTipText("");
        jPanel6.add(jLabel5);
        jPanel6.add(lbDocGia);

        panelInfo.add(jPanel6);

        jPanel7.setPreferredSize(new java.awt.Dimension(400, 163));
        jPanel7.setLayout(new java.awt.GridLayout(2, 2));

        jLabel3.setText("Mã nhân viên");
        jPanel7.add(jLabel3);

        cbNhanVien.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbNhanVienActionPerformed(evt);
            }
        });
        jPanel7.add(cbNhanVien);

        jLabel7.setText("Tên nhân viên");
        jPanel7.add(jLabel7);
        jPanel7.add(lbNhanVien);

        panelInfo.add(jPanel7);

        btnMaMuon.setText("Lấy mã mượn");
        btnMaMuon.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMaMuonActionPerformed(evt);
            }
        });

        lbMaMuon.setText("Mã mượn: ");

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addComponent(btnMaMuon)
                .addGap(18, 18, 18)
                .addComponent(lbMaMuon)
                .addContainerGap(63, Short.MAX_VALUE))
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnMaMuon, javax.swing.GroupLayout.DEFAULT_SIZE, 31, Short.MAX_VALUE)
                    .addComponent(lbMaMuon))
                .addContainerGap())
        );

        panelInfo.add(jPanel11);

        jPanel5.add(panelInfo);

        pChiTietMuonTra.setPreferredSize(new java.awt.Dimension(1013, 103));
        pChiTietMuonTra.setLayout(new javax.swing.BoxLayout(pChiTietMuonTra, javax.swing.BoxLayout.LINE_AXIS));

        jPanel3.setMinimumSize(new java.awt.Dimension(730, 180));

        jLabel11.setText("Chọn sách");

        cbSach.setMaximumSize(new java.awt.Dimension(355, 32767));
        cbSach.setPreferredSize(new java.awt.Dimension(355, 26));
        cbSach.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbSachActionPerformed(evt);
            }
        });

        btnThemSach.setIcon(new javax.swing.ImageIcon(getClass().getResource("/qltv/image/Add-icon.png"))); // NOI18N
        btnThemSach.setText("Thêm sách");
        btnThemSach.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnThemSachActionPerformed(evt);
            }
        });

        btnXoaSach.setIcon(new javax.swing.ImageIcon(getClass().getResource("/qltv/image/Atru.png"))); // NOI18N
        btnXoaSach.setText("Xoá sách");
        btnXoaSach.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnXoaSachActionPerformed(evt);
            }
        });

        btnMuonSach.setText("Mượn sách");
        btnMuonSach.setToolTipText("");
        btnMuonSach.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMuonSachActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGap(53, 53, 53)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(btnThemSach, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(28, 28, 28)
                        .addComponent(btnXoaSach, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(cbSach, javax.swing.GroupLayout.PREFERRED_SIZE, 355, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(lbSoLuongConLai, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(btnMuonSach)
                .addGap(94, 94, 94))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnMuonSach, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(34, 34, 34)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cbSach, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbSoLuongConLai, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnXoaSach)
                            .addComponent(btnThemSach))))
                .addContainerGap(27, Short.MAX_VALUE))
        );

        pChiTietMuonTra.add(jPanel3);

        jPanel5.add(pChiTietMuonTra);

        jPanel1.add(jPanel5);

        jPanel8.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0))));
        jPanel8.setLayout(new javax.swing.BoxLayout(jPanel8, javax.swing.BoxLayout.LINE_AXIS));

        jPanel12.setBorder(javax.swing.BorderFactory.createEmptyBorder(20, 1, 20, 1));
        jPanel12.setMaximumSize(new java.awt.Dimension(50, 30000));
        jPanel12.setMinimumSize(new java.awt.Dimension(50, 100));
        jPanel12.setPreferredSize(new java.awt.Dimension(150, 202));
        jPanel12.setLayout(new javax.swing.BoxLayout(jPanel12, javax.swing.BoxLayout.PAGE_AXIS));

        jLabel4.setText("Ngày mượn");
        jPanel12.add(jLabel4);

        lbNgayMuon.setText("...");
        jPanel12.add(lbNgayMuon);

        jLabel8.setText("Hạn trả");
        jLabel8.setToolTipText("");
        jPanel12.add(jLabel8);

        lbHanTra.setText("...");
        jPanel12.add(lbHanTra);

        jPanel8.add(jPanel12);

        jScrollPane2.setBorder(null);

        tbMuonTra.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tbMuonTra.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbMuonTraMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tbMuonTra);

        jPanel8.add(jScrollPane2);

        jPanel1.add(jPanel8);

        jTabbedPane1.addTab("Quản lý mượn sách", jPanel1);

        jPanel18.setBorder(javax.swing.BorderFactory.createEmptyBorder(20, 1, 1, 20));
        jPanel18.setLayout(new java.awt.BorderLayout());

        jPanel20.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Tìm kiếm thông tin"));
        jPanel20.setMaximumSize(new java.awt.Dimension(33077, 150));
        jPanel20.setMinimumSize(new java.awt.Dimension(210, 150));
        jPanel20.setPreferredSize(new java.awt.Dimension(970, 150));
        jPanel20.setLayout(new javax.swing.BoxLayout(jPanel20, javax.swing.BoxLayout.LINE_AXIS));

        jPanel10.setMaximumSize(new java.awt.Dimension(300, 300));
        jPanel10.setPreferredSize(new java.awt.Dimension(300, 100));

        jLabel15.setText("Nhập mã mượn :");

        btnTimKiem5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/qltv/image/Atim.png"))); // NOI18N
        btnTimKiem5.setText("Tìm kiếm");
        btnTimKiem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTimKiem5ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
            .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel10Layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(tfMaMuon2)
                        .addComponent(jLabel15)
                        .addComponent(btnTimKiem5, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 129, Short.MAX_VALUE)
            .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel10Layout.createSequentialGroup()
                    .addGap(10, 10, 10)
                    .addComponent(jLabel15)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(tfMaMuon2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(btnTimKiem5)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        jPanel20.add(jPanel10);

        jPanel13.setMinimumSize(new java.awt.Dimension(100, 150));

        jLabel6.setText("Tên độc giả");

        lbTenDocGia.setText("...");

        jLabel10.setText("Tên nhân viên");

        lbTenNhanVien.setText("...");

        jLabel13.setText("Ngày mượn");

        jLabel14.setText("Hạn trả");

        lbNgayMuonPhieu.setText("...");

        lbHanTraPhieu.setText("...");

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbTenDocGia, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbTenNhanVien, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(80, 80, 80)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(41, 41, 41)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lbNgayMuonPhieu, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lbHanTraPhieu, javax.swing.GroupLayout.DEFAULT_SIZE, 128, Short.MAX_VALUE))
                .addGap(47, 47, 47))
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, 24, Short.MAX_VALUE)
                        .addComponent(lbTenDocGia, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lbNgayMuonPhieu, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel10)
                    .addComponent(jLabel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lbHanTraPhieu, javax.swing.GroupLayout.DEFAULT_SIZE, 24, Short.MAX_VALUE)
                    .addComponent(lbTenNhanVien, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(73, Short.MAX_VALUE))
        );

        jPanel20.add(jPanel13);

        jPanel18.add(jPanel20, java.awt.BorderLayout.PAGE_START);

        jPanel21.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Chi tiết phiếu mượn"));
        jPanel21.setLayout(new java.awt.BorderLayout());

        jtablehiTietPhieuMuon.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane4.setViewportView(jtablehiTietPhieuMuon);

        jPanel21.add(jScrollPane4, java.awt.BorderLayout.CENTER);

        btnRefresh3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/qltv/image/Alammoi.png"))); // NOI18N
        btnRefresh3.setText("Refresh");
        btnRefresh3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefresh3ActionPerformed(evt);
            }
        });
        jPanel9.add(btnRefresh3);

        btnInPhieu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/qltv/image/Aprint.png"))); // NOI18N
        btnInPhieu.setText("In phiếu");
        btnInPhieu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnInPhieuActionPerformed(evt);
            }
        });
        jPanel9.add(btnInPhieu);

        jPanel21.add(jPanel9, java.awt.BorderLayout.SOUTH);

        jPanel18.add(jPanel21, java.awt.BorderLayout.CENTER);

        jTabbedPane1.addTab("Chi tiết phiếu mượn", jPanel18);

        add(jTabbedPane1);
    }// </editor-fold>//GEN-END:initComponents

    private void jTabbedPane1ComponentAdded(java.awt.event.ContainerEvent evt) {//GEN-FIRST:event_jTabbedPane1ComponentAdded
        // TODO add your handling code here:
    }//GEN-LAST:event_jTabbedPane1ComponentAdded

    private void btnInPhieuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnInPhieuActionPerformed
        
    }//GEN-LAST:event_btnInPhieuActionPerformed

    private void btnRefresh3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefresh3ActionPerformed
        updateChiTietPhieuMuon();
    }//GEN-LAST:event_btnRefresh3ActionPerformed

    private void btnTimKiem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTimKiem5ActionPerformed
        loadDLSearch();
    }//GEN-LAST:event_btnTimKiem5ActionPerformed

    private void cbDocgiaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbDocgiaActionPerformed
        // TODO add your handling code here:
        cbDocGiaClicked();
    }//GEN-LAST:event_cbDocgiaActionPerformed

    private void cbNhanVienActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbNhanVienActionPerformed
        // TODO add your handling code here:
        cbNhanVienClicked();
    }//GEN-LAST:event_cbNhanVienActionPerformed

    private void cbSachActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbSachActionPerformed
        // TODO add your handling code here:
        cbSachClicked();
    }//GEN-LAST:event_cbSachActionPerformed

    private void btnMaMuonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMaMuonActionPerformed
        // TODO add your handling code here:
        btnMaMuonClicked();
    }//GEN-LAST:event_btnMaMuonActionPerformed

    private void btnThemSachActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnThemSachActionPerformed
        // TODO add your handling code here:
        btnThemSach();
    }//GEN-LAST:event_btnThemSachActionPerformed

    private void btnXoaSachActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnXoaSachActionPerformed
        // TODO add your handling code here:
        btnXoaSach();
    }//GEN-LAST:event_btnXoaSachActionPerformed

    private void tbMuonTraMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbMuonTraMouseClicked
        // TODO add your handling code here:
        btnXoaSach.setEnabled(true);
    }//GEN-LAST:event_tbMuonTraMouseClicked

    private void btnMuonSachActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMuonSachActionPerformed
        // TODO add your handling code here:
        btnMuonSach();
    }//GEN-LAST:event_btnMuonSachActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnInPhieu;
    private javax.swing.JButton btnMaMuon;
    private javax.swing.JButton btnMuonSach;
    private javax.swing.JButton btnRefresh3;
    private javax.swing.JButton btnThemSach;
    private javax.swing.JButton btnTimKiem5;
    private javax.swing.JButton btnXoaSach;
    private javax.swing.JComboBox<DocGia> cbDocgia;
    private javax.swing.JComboBox<NhanVien> cbNhanVien;
    private javax.swing.JComboBox<Sach> cbSach;
    private javax.swing.JButton jButton1;
    private javax.swing.JDesktopPane jDesktopPane1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel21;
    private javax.swing.JPanel jPanel22;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable jtablehiTietPhieuMuon;
    private javax.swing.JLabel lbDocGia;
    private javax.swing.JLabel lbHanTra;
    private javax.swing.JLabel lbHanTraPhieu;
    private javax.swing.JLabel lbMaMuon;
    private javax.swing.JLabel lbNgayMuon;
    private javax.swing.JLabel lbNgayMuonPhieu;
    private javax.swing.JLabel lbNhanVien;
    private javax.swing.JLabel lbSoLuongConLai;
    private javax.swing.JLabel lbTenDocGia;
    private javax.swing.JLabel lbTenNhanVien;
    private javax.swing.JPanel pChiTietMuonTra;
    private javax.swing.JPanel panelInfo;
    private javax.swing.JTable tbMuonTra;
    private javax.swing.JTextField tfMaMuon2;
    // End of variables declaration//GEN-END:variables
}
