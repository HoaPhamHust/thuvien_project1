/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qltv.model.dao.imp;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import qltv.common.JdbcTemplate;
import qltv.model.dto.DocGia;

/**
 *
 * @author HOA
 */
public class DocGiaDaoImpl extends CommonDaoImpl<DocGia>{
    
    private JdbcTemplate.RowCallBackHandler rowCallBackHandler() {
        return new JdbcTemplate.RowCallBackHandler<DocGia>() {
            @Override
            public DocGia processRow(ResultSet rs) throws SQLException {
                DocGia docGia = new DocGia();
                docGia.setMaDocGia(rs.getString("MaDG"));
                docGia.setTenDocGia(rs.getString("TenDG"));
                docGia.setHanSuDung(rs.getDate("HanSuDung"));
                docGia.setNgaySinh(rs.getDate("NgaySinh"));
                docGia.setDiaChi(rs.getString("DiaChi"));
                docGia.setSoDienThoai(rs.getString("SoDienThoai"));
                docGia.setGioiTinh(rs.getString("GioiTinh"));
                
                return docGia;
            }
        };
    }
    
}
