/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qltv.model.dao.imp;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import qltv.common.JdbcTemplate;
import qltv.model.dto.MuonTra;

/**
 *
 * @author HOA
 */
public class MuonTraDaoImp extends CommonDaoImpl<MuonTra>{

    @Override
    public MuonTra findById(int id) {
        return (MuonTra) JdbcTemplate.singleQuery("SELECT * FROM MuonTra WHERE id = ?", (PreparedStatement pstmt) -> {
            pstmt.setInt(1, id);
        }, rowCallBackHandler());
    }

    @Override
    public int delete(MuonTra t) {
        return JdbcTemplate.update("DELETE FROM MuonTra WHERE MaMuon = ?", (PreparedStatement ps) -> {
            ps.setString(1, t.getMaMuon());
        });
    }
    
    public void test() {
        
    }
    
     private JdbcTemplate.RowCallBackHandler rowCallBackHandler() {
         return (JdbcTemplate.RowCallBackHandler<MuonTra>) (ResultSet rs) -> {
             MuonTra muonTra = new MuonTra();
             muonTra.setMaMuon(rs.getString("MaMuon"));
             muonTra.setMaDocGiaMuon(rs.getString("MaDocGiaMuon"));
             muonTra.setMaNhanVienMuon("MaNhanVienMuon");
             muonTra.setHanTra(rs.getDate("HanTra"));
             muonTra.setNgayMuon(rs.getDate("NgayMuon"));
             muonTra.setTrangThai(rs.getNString("TrangThai"));
             
             return muonTra;
         };
     }
}
