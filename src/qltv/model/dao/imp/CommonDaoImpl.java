/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qltv.model.dao.imp;

import java.util.List;
import qltv.model.dao.ICommonDao;

/**
 *
 * @author HOA
 */
public abstract class CommonDaoImpl<T> implements ICommonDao<T> {

    @Override
    public int[] batch(@SuppressWarnings("unchecked") T... instances) {
        return null;
    }

    @Override
    public int save(T t) {
        return 0;
    }

    @Override
    public int update(T t) {
        return 0;
    }

    @Override
    public int delete(int id) {
        return 0;
    }

    @Override
    public int delete(T t) {
        return 0;
    }

    @Override
    public T findById(int id) {
        return null;
    }

    @Override
    public List<T> findAll() {
        return null;
    }

    @Override
    public List<T> findByInstance(T t) {
        return null;
    }

    @Override
    public List<T> find(int start, int count, String order) {
        return null;
    }

    @Override
    public List<T> findByConditions(String... conditions) {
        return null;
    }

    @Override
    public int delete(String... conditions) {
        return 0;
    }

}
