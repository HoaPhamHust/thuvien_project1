/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qltv.model.dao.imp;

import qltv.common.JdbcTemplate;
import qltv.model.dto.ChiTietMuon;

/**
 *
 * @author HOA
 */
public class ChiTietMuonDaoImpl extends CommonDaoImpl<ChiTietMuon> {

    @Override
    public int delete(String... conditions) {
        String sql = "DELETE FROM ChiTietMuon WHERE ";
        for (int i = 0; i < conditions.length / 2 * 2; i+=2) {
            sql += String.format("%s = '%s'", conditions[i], conditions[i+1]);
        }
        return JdbcTemplate.update(sql, null);
    }

    @Override
    public int[] batch(ChiTietMuon... instances) {
        return super.batch(instances); //To change body of generated methods, choose Tools | Templates.
    }
    
    

}
