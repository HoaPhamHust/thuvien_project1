/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qltv.model.dao.imp;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import qltv.common.JdbcTemplate;
import qltv.model.dto.Sach;

/**
 *
 * @author HOA
 */
public class SachDaoImpl extends CommonDaoImpl<Sach>{

    @Override
    public int update(Sach t) {
        String sql = "UPDATE Sach SET TenSach = ?, TacGia = ?, TheLoai = ?, NXB = ?, SoLuong = ?, DonGia = ? WHERE MaSach = ?";
        return JdbcTemplate.update(sql, (PreparedStatement ps) -> {
            ps.setNString(1, t.getTenSach());
            ps.setNString(2, t.getTacGia());
            ps.setNString(3, t.getTheLoai());
            ps.setNString(4, t.getNxb());
            ps.setInt(5, t.getSoLuong());
            ps.setFloat(6, t.getDonGia());
            ps.setString(7, t.getMaSach());
        });
    }
    
    
    
    JdbcTemplate.RowCallBackHandler rowCallBackHandler () {
        return new JdbcTemplate.RowCallBackHandler<Sach>() {
            @Override
            public Sach processRow(ResultSet rs) throws SQLException {
                Sach sach = new Sach();
                sach.setMaSach(rs.getString("MaSach"));
                sach.setTenSach(rs.getNString("TenSach"));
                sach.setTacGia(rs.getNString("TacGia"));
                sach.setTheLoai(rs.getNString("TheLoai"));
                sach.setNxb(rs.getNString("NXB"));
                sach.setSoLuong(rs.getInt("SoLuong"));
                sach.setDonGia(rs.getFloat("DonGia"));
                
                return sach;
            }
        };
    }
}
