/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qltv.model.dao;

import java.util.WeakHashMap;
import qltv.model.dao.imp.ChiTietMuonDaoImpl;
import qltv.model.dao.imp.MuonTraDaoImp;
import qltv.model.dao.imp.SachDaoImpl;

/**
 *
 * @author HOA
 */
@SuppressWarnings("rawtypes")
public final class DaoFactory {
	private final static WeakHashMap<String, ICommonDao> map = new WeakHashMap<>();
	
	public static ICommonDao getDao(String name){
		ICommonDao dao = map.get(name);
		if(dao != null)
			return dao;
		return createDao(name);
	}

	private synchronized static ICommonDao createDao(String name) {
		ICommonDao dao = null;
		if("MuonTra".equals(name)){
			dao = new MuonTraDaoImp();
			map.put(name, dao);
		}
		if("ChiTietMuon".equals(name)){
			dao = new ChiTietMuonDaoImpl();
			map.put(name, dao);
		}
		if("Sach".equals(name)){
			dao = new SachDaoImpl();
			map.put(name, dao);
		}
//		if("order".equals(name)){
//			dao = new OrderDaoImpl();
//			map.put(name, dao);
//		}
//		if("orderItem".equals(name)){
//			dao = new OrderItemDaoImpl();
//			map.put(name, dao);
//		}
//		if("bookChapter".equals(name)){
//			dao = new BookChapterDaoImpl();
//			map.put(name, dao);
//		}
		return dao;
	}
}
