/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qltv.model.dao;

/**
 *
 * @author HOA
 */
import java.util.List;

public interface ICommonDao<T> {
	int save(T t);
	int update(T t);
	int[] batch(@SuppressWarnings("unchecked") T ...instances);
	int delete(int id);
	int delete(T t);
        int delete(String...conditions);
	T findById(int id);
	List<T> findAll();
	List<T> findByInstance(T t);
	List<T> find(int start,int count,String order);
	List<T> findByConditions(String...conditions);
}
