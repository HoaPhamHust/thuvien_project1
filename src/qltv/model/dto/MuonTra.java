/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qltv.model.dto;
import java.util.Date;

public class MuonTra {
    private String maMuon;
    private String maDocGiaMuon;
    private String maNhanVienMuon;
    private Date ngayMuon;
    private Date hanTra;
    private String trangThai;

    public String getMaMuon() {
        return maMuon;
    }

    public void setMaMuon(String maMuon) {
        this.maMuon = maMuon;
    }

    public String getMaDocGiaMuon() {
        return maDocGiaMuon;
    }

    public void setMaDocGiaMuon(String maDocGiaMuon) {
        this.maDocGiaMuon = maDocGiaMuon;
    }

    public String getMaNhanVienMuon() {
        return maNhanVienMuon;
    }

    public void setMaNhanVienMuon(String maNhanVienMuon) {
        this.maNhanVienMuon = maNhanVienMuon;
    }

    public Date getNgayMuon() {
        return ngayMuon;
    }

    public void setNgayMuon(Date ngayMuon) {
        this.ngayMuon = ngayMuon;
    }

    public Date getHanTra() {
        return hanTra;
    }

    public void setHanTra(Date hanTra) {
        this.hanTra = hanTra;
    }

    public String getTrangThai() {
        return trangThai;
    }

    public void setTrangThai(String trangThai) {
        this.trangThai = trangThai;
    }
    
    

    public MuonTra() {
    }

    public MuonTra(String maMuon) {
        this.maMuon = maMuon;
    }

    public MuonTra(String maMuon, String maDocGiaMuon, String tenDocGiaMuon, Date ngayMuon, Date hanTra, Date ngayTra, String trangThai, float tienPhat) {
        this.maMuon = maMuon;
        this.maDocGiaMuon = maDocGiaMuon;
        this.ngayMuon = ngayMuon;
        this.hanTra = hanTra;
        this.trangThai = trangThai;
    }

    public MuonTra(String maMuon, String maNhanVienMuon) {
        this.maMuon = maMuon;
        this.maNhanVienMuon = maNhanVienMuon;
    }

    public MuonTra(String maMuon, String maDocGiaMuon, String tenDocGiaMuon, Date ngayMuon, Date hanTra, Date ngayTra, String trangThai, float tienPhat, String maNhanVienMuon, String tenSachMuon) {
        this.maMuon = maMuon;
        this.maDocGiaMuon = maDocGiaMuon;
        this.ngayMuon = ngayMuon;
        this.hanTra = hanTra;
        this.trangThai = trangThai;
        this.maNhanVienMuon = maNhanVienMuon;
    }
    
    
}
