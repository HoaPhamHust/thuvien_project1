/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qltv.controler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import qltv.common.JdbcTemplate;
import qltv.model.dto.DocGia;
import qltv.model.dto.NhanVien;
import qltv.util.ConvertDate;
//import qltv.util.DatabaseConnection;
import qltv.view.GUIQuanLyDocGia;
import qltv.view.GUIQuanLyNhanVien;

/**
 *
 * @author hoaph
 */
public class CNhanVien {
    
    Connection con = null;
    Statement st = null;
    ResultSet rs = null;
    PreparedStatement ps = null;
    
    

    public CNhanVien() {

    }

    // lay ra toan bo danh sach doc gia 
    public List<NhanVien> selectAll() {
        try {

            rs = JdbcTemplate.query("SELECT * FROM NhanVien;");
            List<NhanVien> nhanVienList = new ArrayList<NhanVien>();
            while (rs.next()) {
                NhanVien nhanVien = new NhanVien();
                nhanVien.setMaNhanVien(rs.getString("MaNV"));
                nhanVien.setTenNhanVien(rs.getString("TenNV"));
                nhanVien.setNgaySinh(rs.getDate("NgaySinh"));
                nhanVien.setDiaChi(rs.getString("DiaChi"));
                nhanVien.setSoDienThoai(rs.getString("SoDienThoai"));

                nhanVienList.add(nhanVien);
            }

            return nhanVienList;
        } catch (SQLException ex) {
            Logger.getLogger(CNhanVien.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex.getMessage());
        }
        return null;

    }

    // lay r danh sach doc gia khi thuc hien tim kiem
    // columName = {ma, ten, dia chi, so dien thoai, gioi tinh}
    public List<NhanVien> search(NhanVien nhanVien, String columnName) throws SQLException {
        try {
            String sql = "SELECT * FROM NhanVien WHERE 1 = 1 ";
            switch (columnName) {
                case "Mã nhân viên": {
                    sql += " AND MaNV LIKE N'%" + nhanVien.getMaNhanVien() + "%';";

                    break;
                }

                case "Tên nhân viên": {
                    sql += " AND TenNV LIKE N'%" + nhanVien.getTenNhanVien() + "%';";

                    break;
                }
                case "Địa chỉ": {
                    sql += " AND DiaChi LIKE N'%" + nhanVien.getDiaChi() + "%';";
                    break;
                }
                case "Số điện thoại": {
                    sql += " AND SoDienThoai LIKE N'%" + nhanVien.getSoDienThoai() + "%';";
                    break;
                }
                
                case "None": {
                    break;
                }
            }
            System.out.println(sql);
            rs = st.executeQuery(sql);

            List<NhanVien> nhanVienList = new ArrayList<NhanVien>();
            while (rs.next()) {
                NhanVien dg = new NhanVien();

                dg.setMaNhanVien(rs.getString("MaNV"));
                dg.setTenNhanVien(rs.getString("TenNV"));
                dg.setNgaySinh(rs.getDate("NgaySinh"));
                dg.setDiaChi(rs.getString("DiaChi"));
                dg.setSoDienThoai(rs.getString("SoDienThoai"));

                nhanVienList.add(dg);
            }

            return nhanVienList;
        } catch (SQLException ex) {
            Logger.getLogger(CNhanVien.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            con.close();
        }

        return null;

    }

    // insert nhanVien
    public boolean insert(NhanVien nhanVien) {

        try {
            String sql = "INSERT INTO NhanVien(TenNV, NgaySinh, DiaChi, SoDienThoai) VALUES(?, ?, ?, ?)";
            ps = con.prepareStatement(sql);

            ps.setString(1, nhanVien.getTenNhanVien());
            ps.setDate(2, ConvertDate.convertDate(nhanVien.getNgaySinh()));
            ps.setString(3, nhanVien.getDiaChi());
            ps.setString(4, nhanVien.getSoDienThoai());

            System.out.println(sql);
            return ps.execute();
        } catch (SQLException ex) {
            Logger.getLogger(CNhanVien.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

// update DL
    public boolean update(NhanVien nhanVien) {
        try {
            String sql = "UPDATE NhanVien SET TenNV = ?, NgaySinh = ?, DiaChi = ?, SoDienThoai = ? WHERE MaNV = ? ";

            ps = con.prepareStatement(sql);

            ps.setString(1, nhanVien.getTenNhanVien());
            ps.setDate(2, ConvertDate.convertDate(nhanVien.getNgaySinh()));
            ps.setString(3, nhanVien.getDiaChi());
            ps.setString(4, nhanVien.getSoDienThoai());
            ps.setString(5, nhanVien.getMaNhanVien());

            System.out.println(sql);
            return ps.execute();

        } catch (SQLException ex) {
            Logger.getLogger(CNhanVien.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(CNhanVien.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return false;

    }
    
    // Xoa nhanVien
    public boolean delete(NhanVien nhanVien){
        
        try {
            String sql = "DELETE FROM NhanVien WHERE MaNV = ? ";
            ps = con.prepareStatement(sql);
            ps.setString(1, nhanVien.getMaNhanVien());
            
            return ps.execute();
        } catch (SQLException ex) {
            Logger.getLogger(CNhanVien.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(GUIQuanLyNhanVien.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return false;
    }
}
