/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qltv.controler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import qltv.common.JdbcTemplate;
import qltv.model.dto.Sach;
import qltv.model.dto.Sach;
import qltv.util.*;
import qltv.view.GUIQuanLySach;

public class CSach {

    Connection con = null;
    Statement st = null;
    ResultSet rs = null;
    PreparedStatement ps = null;

    public CSach() {
    }

    
    // lay ra toan bo danh sach doc gia 
    public List<Sach> selectAll() {
        try {
            rs = JdbcTemplate.query("SELECT * FROM Sach;");
            List<Sach> sachList = new ArrayList<Sach>();
            while (rs.next()) {
                Sach sach = new Sach();
                sach.setMaSach(rs.getString("MaSach"));
                sach.setTenSach(rs.getString("TenSach"));
                sach.setNxb(rs.getString("NXB"));
                sach.setTacGia(rs.getString("TacGia"));
                sach.setTheLoai(rs.getString("TheLoai"));
                sach.setDonGia(rs.getFloat("DonGia"));
                sach.setSoLuong(rs.getInt("SoLuong"));

                sachList.add(sach);
            }

            return sachList;
        } catch (SQLException ex) {
            Logger.getLogger(CSach.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex.getMessage());
        }
        return null;

    }

    // lay r danh sach doc gia khi thuc hien tim kiem
    // columName = {ma, ten, dia chi, so dien thoai, gioi tinh}
    public List<Sach> search(Sach sach, String columnName) throws SQLException {
        try {
            String sql = "SELECT * FROM Sach WHERE 1 = 1 ";
            switch (columnName) {
                case "Mã sách": {
                    sql += " AND MaSach LIKE N'%" + sach.getMaSach() + "%';";

                    break;
                }

                case "Tên sách": {
                    sql += " AND TenSach LIKE N'%" + sach.getTenSach() + "%';";

                    break;
                }
                case "Nhà xuất bản": {
                    sql += " AND NXB LIKE N'%" + sach.getNxb() + "%';";
                    break;
                }
                case "Tác giả": {
                    sql += " AND TacGia LIKE N'%" + sach.getTacGia() + "%';";
                    break;
                }
                case "Thể loại": {
                    sql += " AND TheLoai LIKE N'%" + sach.getTheLoai() + "%';";
                    break;
                }

                case "None": {
                    break;
                }
            }
            System.out.println(sql);
            rs = JdbcTemplate.query(sql);

            List<Sach> sachList = new ArrayList<Sach>();
            while (rs.next()) {
                Sach s = new Sach();

                s.setMaSach(rs.getString("MaSach"));
                s.setTenSach(rs.getString("TenSach"));
                s.setNxb(rs.getString("NXB"));
                s.setTacGia(rs.getString("TacGia"));
                s.setTheLoai(rs.getString("TheLoai"));
                s.setDonGia(rs.getFloat("DonGia"));
                s.setSoLuong(rs.getInt("SoLuong"));

                sachList.add(s);
            }

            return sachList;
        } catch (SQLException ex) {
            Logger.getLogger(CSach.class.getName()).log(Level.SEVERE, null, ex);
        } 

        return null;

    }

    // insert sach
    public boolean insert(Sach sach) {

        try {
            String sql = "INSERT INTO Sach(TenSach, NXB, TacGia, TheLoai, DonGia, SoLuong) VALUES(?, ?, ?, ?, ?, ?)";
            JdbcTemplate.query(sql, (PreparedStatement ps1) -> {
                ps1.setString(1, sach.getTenSach());
                ps1.setString(2, sach.getNxb());
                ps1.setString(3, sach.getTacGia());
                ps1.setString(4, sach.getTheLoai());
                ps1.setFloat(5, sach.getDonGia());
                ps1.setInt(6, sach.getSoLuong());
            });

        } catch (SQLException ex) {
            Logger.getLogger(CSach.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

// update DL
    public boolean update(Sach sach) {
        try {
            String sql = "UPDATE Sach SET TenSach = ?, NXB = ?, TacGia = ?, TheLoai = ?, DonGia = ?, SoLuong = ? WHERE MaSach = ? ";

            JdbcTemplate.query(sql, (PreparedStatement ps1) -> {
                ps1.setString(1, sach.getTenSach());
                ps1.setString(2, sach.getNxb());
                ps1.setString(3, sach.getTacGia());
                ps1.setString(4, sach.getTheLoai());
                ps1.setFloat(5, sach.getDonGia());
                ps1.setInt(6, sach.getSoLuong());
                ps1.setString(7, sach.getMaSach());
            });

            return ps.execute();

        } catch (SQLException ex) {
            Logger.getLogger(CSach.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return false;

    }

    // Xoa sach
    public boolean delete(Sach sach) {

        try {
            String sql = "DELETE FROM Sach WHERE MaSach = ? ";
            ps = con.prepareStatement(sql);
            ps.setString(1, sach.getMaSach());

            return ps.execute();
        } catch (SQLException ex) {
            Logger.getLogger(CSach.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Xóa sách không thành công!");
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(GUIQuanLySach.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return false;
    }


}
