/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qltv.common;

/**
 *
 * @author HOA
 */
import com.microsoft.sqlserver.jdbc.SQLServerConnectionPoolDataSource;
import com.microsoft.sqlserver.jdbc.SQLServerDataSource;
import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

public final class ConnectionManager {

    private final static ThreadLocal<Connection> LOCAL = new ThreadLocal<>();
    private static DataSource dataSource;
    private static final String url = "jdbc:sqlserver://localhost:1433";
    private static final String driverName = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
    private static final String username = "sa";
    private static final String password = "1";
    
    public static ConnectionManager instance = new ConnectionManager();

    private ConnectionManager() {
        SQLServerDataSource dataSource = new SQLServerDataSource();
        dataSource.setURL(url);
        dataSource.setUser(username);
        dataSource.setPassword(password);
        dataSource.setDatabaseName("LibTest2");
        ConnectionManager.setDataSource(dataSource);
    }

    public static void setDataSource(DataSource dataSource) {
        ConnectionManager.dataSource = dataSource;
    }

    public static Connection getConnection() throws SQLException {
        Connection conn = LOCAL.get();
        if (null != conn) {
            return conn;
        }
        conn = dataSource.getConnection();
        LOCAL.set(conn);
        return conn;
    }

    public static void release() {
        Connection conn = LOCAL.get();
        if (null != conn) {
            DBUtil.release(conn);
            LOCAL.remove();
        }
    }

}
