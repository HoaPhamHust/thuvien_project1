
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import qltv.common.ConnectionManager;
import qltv.model.dto.DocGia;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author HOA
 */
public class Main {
    public static void main(String[] args) {
        try {
            Connection connection = ConnectionManager.getConnection();
            System.out.println("Main.main()");
        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
